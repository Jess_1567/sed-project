/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
extern char *IEEE_P_2592010699;
extern char *STD_STANDARD;
extern char *IEEE_P_3620187407;
static const char *ng3 = "Function to_bcd ended without a return statement";
static const char *ng4 = "C:/.Xilinx/Bueno para Test-Bench/blackJack/blackJack_game.vhd";

char *ieee_p_3620187407_sub_767668596_3965413181(char *, char *, char *, char *, char *, char *);


char *work_a_0519386517_3212880686_sub_4141168059_3057020925(char *t1, char *t2, char *t3)
{
    char t4[368];
    char t5[24];
    char t6[16];
    char t11[16];
    char t24[16];
    char t30[8];
    char t37[8];
    char t46[16];
    char t66[16];
    char t68[16];
    char t69[16];
    char t70[16];
    char *t0;
    char *t7;
    char *t8;
    int t9;
    unsigned int t10;
    char *t12;
    char *t13;
    int t14;
    unsigned int t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t25;
    char *t26;
    int t27;
    char *t28;
    char *t29;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t38;
    char *t39;
    char *t40;
    unsigned char t41;
    char *t42;
    char *t43;
    char *t44;
    char *t47;
    char *t48;
    int t49;
    unsigned char t50;
    char *t52;
    char *t53;
    int t54;
    unsigned int t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    int t61;
    unsigned int t62;
    unsigned int t63;
    int t64;
    int t65;
    unsigned char t67;
    int t71;
    unsigned int t72;

LAB0:    t7 = (t6 + 0U);
    t8 = (t7 + 0U);
    *((int *)t8) = 7;
    t8 = (t7 + 4U);
    *((int *)t8) = 0;
    t8 = (t7 + 8U);
    *((int *)t8) = -1;
    t9 = (0 - 7);
    t10 = (t9 * -1);
    t10 = (t10 + 1);
    t8 = (t7 + 12U);
    *((unsigned int *)t8) = t10;
    t8 = (t6 + 12U);
    t10 = *((unsigned int *)t8);
    t10 = (t10 * 1U);
    t12 = (t11 + 0U);
    t13 = (t12 + 0U);
    *((int *)t13) = 7;
    t13 = (t12 + 4U);
    *((int *)t13) = 0;
    t13 = (t12 + 8U);
    *((int *)t13) = -1;
    t14 = (0 - 7);
    t15 = (t14 * -1);
    t15 = (t15 + 1);
    t13 = (t12 + 12U);
    *((unsigned int *)t13) = t15;
    t13 = (t4 + 4U);
    t16 = ((IEEE_P_2592010699) + 4024);
    t17 = (t13 + 88U);
    *((char **)t17) = t16;
    t18 = (char *)alloca(t10);
    t19 = (t13 + 56U);
    *((char **)t19) = t18;
    memcpy(t18, t3, t10);
    t20 = (t13 + 64U);
    *((char **)t20) = t11;
    t21 = (t13 + 80U);
    *((unsigned int *)t21) = t10;
    t22 = xsi_get_transient_memory(8U);
    memset(t22, 0, 8U);
    t23 = t22;
    memset(t23, (unsigned char)2, 8U);
    t25 = (t24 + 0U);
    t26 = (t25 + 0U);
    *((int *)t26) = 7;
    t26 = (t25 + 4U);
    *((int *)t26) = 0;
    t26 = (t25 + 8U);
    *((int *)t26) = -1;
    t27 = (0 - 7);
    t15 = (t27 * -1);
    t15 = (t15 + 1);
    t26 = (t25 + 12U);
    *((unsigned int *)t26) = t15;
    t26 = (t4 + 124U);
    t28 = ((IEEE_P_2592010699) + 4024);
    t29 = (t26 + 88U);
    *((char **)t29) = t28;
    t31 = (t26 + 56U);
    *((char **)t31) = t30;
    memcpy(t30, t22, 8U);
    t32 = (t26 + 64U);
    *((char **)t32) = t24;
    t33 = (t26 + 80U);
    *((unsigned int *)t33) = 8U;
    t34 = (t4 + 244U);
    t35 = ((STD_STANDARD) + 384);
    t36 = (t34 + 88U);
    *((char **)t36) = t35;
    t38 = (t34 + 56U);
    *((char **)t38) = t37;
    *((int *)t37) = 0;
    t39 = (t34 + 80U);
    *((unsigned int *)t39) = 4U;
    t40 = (t5 + 4U);
    t41 = (t3 != 0);
    if (t41 == 1)
        goto LAB3;

LAB2:    t42 = (t5 + 12U);
    *((char **)t42) = t6;
    t43 = (t13 + 56U);
    t44 = *((char **)t43);
    t43 = (t1 + 15606);
    t47 = (t46 + 0U);
    t48 = (t47 + 0U);
    *((int *)t48) = 0;
    t48 = (t47 + 4U);
    *((int *)t48) = 7;
    t48 = (t47 + 8U);
    *((int *)t48) = 1;
    t49 = (7 - 0);
    t15 = (t49 * 1);
    t15 = (t15 + 1);
    t48 = (t47 + 12U);
    *((unsigned int *)t48) = t15;
    t50 = ieee_std_logic_unsigned_greater_stdv_stdv(IEEE_P_3620187407, t44, t11, t43, t46);
    if (t50 != 0)
        goto LAB4;

LAB6:    t9 = 0;
    t14 = 7;

LAB8:    if (t9 <= t14)
        goto LAB9;

LAB11:    t7 = (t26 + 56U);
    t8 = *((char **)t7);
    t7 = (t24 + 12U);
    t10 = *((unsigned int *)t7);
    t10 = (t10 * 1U);
    t0 = xsi_get_transient_memory(t10);
    memcpy(t0, t8, t10);
    t12 = (t24 + 0U);
    t9 = *((int *)t12);
    t16 = (t24 + 4U);
    t14 = *((int *)t16);
    t17 = (t24 + 8U);
    t27 = *((int *)t17);
    t19 = (t2 + 0U);
    t20 = (t19 + 0U);
    *((int *)t20) = t9;
    t20 = (t19 + 4U);
    *((int *)t20) = t14;
    t20 = (t19 + 8U);
    *((int *)t20) = t27;
    t49 = (t14 - t9);
    t15 = (t49 * t27);
    t15 = (t15 + 1);
    t20 = (t19 + 12U);
    *((unsigned int *)t20) = t15;

LAB1:    return t0;
LAB3:    *((char **)t40) = t3;
    goto LAB2;

LAB4:    t48 = (t1 + 15614);
    t0 = xsi_get_transient_memory(8U);
    memcpy(t0, t48, 8U);
    t52 = (t2 + 0U);
    t53 = (t52 + 0U);
    *((int *)t53) = 1;
    t53 = (t52 + 4U);
    *((int *)t53) = 8;
    t53 = (t52 + 8U);
    *((int *)t53) = 1;
    t54 = (8 - 1);
    t15 = (t54 * 1);
    t15 = (t15 + 1);
    t53 = (t52 + 12U);
    *((unsigned int *)t53) = t15;
    goto LAB1;

LAB5:    xsi_error(ng3);
    t0 = 0;
    goto LAB1;

LAB7:    goto LAB5;

LAB9:    t7 = (t26 + 56U);
    t8 = *((char **)t7);
    t7 = (t24 + 0U);
    t27 = *((int *)t7);
    t10 = (t27 - 6);
    t15 = (t10 * 1U);
    t55 = (0 + t15);
    t12 = (t8 + t55);
    t49 = (0 - 6);
    t56 = (t49 * -1);
    t56 = (t56 + 1);
    t57 = (1U * t56);
    t16 = xsi_get_transient_memory(t57);
    memcpy(t16, t12, t57);
    t17 = (t26 + 56U);
    t19 = *((char **)t17);
    t17 = (t24 + 0U);
    t54 = *((int *)t17);
    t58 = (t54 - 7);
    t59 = (t58 * 1U);
    t60 = (0 + t59);
    t20 = (t19 + t60);
    t61 = (0 - 6);
    t62 = (t61 * -1);
    t62 = (t62 + 1);
    t63 = (1U * t62);
    memcpy(t20, t16, t63);
    t7 = (t13 + 56U);
    t8 = *((char **)t7);
    t7 = (t11 + 0U);
    t27 = *((int *)t7);
    t12 = (t11 + 8U);
    t49 = *((int *)t12);
    t54 = (7 - t27);
    t10 = (t54 * t49);
    t15 = (1U * t10);
    t55 = (0 + t15);
    t16 = (t8 + t55);
    t41 = *((unsigned char *)t16);
    t17 = (t26 + 56U);
    t19 = *((char **)t17);
    t17 = (t24 + 0U);
    t61 = *((int *)t17);
    t20 = (t24 + 8U);
    t64 = *((int *)t20);
    t65 = (0 - t61);
    t56 = (t65 * t64);
    t57 = (1U * t56);
    t58 = (0 + t57);
    t21 = (t19 + t58);
    *((unsigned char *)t21) = t41;
    t7 = (t13 + 56U);
    t8 = *((char **)t7);
    t7 = (t11 + 0U);
    t27 = *((int *)t7);
    t10 = (t27 - 6);
    t15 = (t10 * 1U);
    t55 = (0 + t15);
    t12 = (t8 + t55);
    t49 = (0 - 6);
    t56 = (t49 * -1);
    t56 = (t56 + 1);
    t57 = (1U * t56);
    t16 = xsi_get_transient_memory(t57);
    memcpy(t16, t12, t57);
    t17 = (t13 + 56U);
    t19 = *((char **)t17);
    t17 = (t11 + 0U);
    t54 = *((int *)t17);
    t58 = (t54 - 7);
    t59 = (t58 * 1U);
    t60 = (0 + t59);
    t20 = (t19 + t60);
    t61 = (0 - 6);
    t62 = (t61 * -1);
    t62 = (t62 + 1);
    t63 = (1U * t62);
    memcpy(t20, t16, t63);
    t7 = (t13 + 56U);
    t8 = *((char **)t7);
    t7 = (t11 + 0U);
    t27 = *((int *)t7);
    t12 = (t11 + 8U);
    t49 = *((int *)t12);
    t54 = (0 - t27);
    t10 = (t54 * t49);
    t15 = (1U * t10);
    t55 = (0 + t15);
    t16 = (t8 + t55);
    *((unsigned char *)t16) = (unsigned char)2;
    t50 = (t9 < 7);
    if (t50 == 1)
        goto LAB15;

LAB16:    t41 = (unsigned char)0;

LAB17:    if (t41 != 0)
        goto LAB12;

LAB14:
LAB13:    t50 = (t9 < 7);
    if (t50 == 1)
        goto LAB21;

LAB22:    t41 = (unsigned char)0;

LAB23:    if (t41 != 0)
        goto LAB18;

LAB20:
LAB19:
LAB10:    if (t9 == t14)
        goto LAB11;

LAB24:    t27 = (t9 + 1);
    t9 = t27;
    goto LAB8;

LAB12:    t21 = (t26 + 56U);
    t22 = *((char **)t21);
    t21 = (t24 + 0U);
    t61 = *((int *)t21);
    t56 = (t61 - 3);
    t57 = (t56 * 1U);
    t58 = (0 + t57);
    t23 = (t22 + t58);
    t25 = (t69 + 0U);
    t28 = (t25 + 0U);
    *((int *)t28) = 3;
    t28 = (t25 + 4U);
    *((int *)t28) = 0;
    t28 = (t25 + 8U);
    *((int *)t28) = -1;
    t64 = (0 - 3);
    t59 = (t64 * -1);
    t59 = (t59 + 1);
    t28 = (t25 + 12U);
    *((unsigned int *)t28) = t59;
    t28 = (t1 + 15626);
    t31 = (t70 + 0U);
    t32 = (t31 + 0U);
    *((int *)t32) = 0;
    t32 = (t31 + 4U);
    *((int *)t32) = 3;
    t32 = (t31 + 8U);
    *((int *)t32) = 1;
    t65 = (3 - 0);
    t59 = (t65 * 1);
    t59 = (t59 + 1);
    t32 = (t31 + 12U);
    *((unsigned int *)t32) = t59;
    t32 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t68, t23, t69, t28, t70);
    t33 = (t26 + 56U);
    t35 = *((char **)t33);
    t33 = (t24 + 0U);
    t71 = *((int *)t33);
    t59 = (t71 - 3);
    t60 = (t59 * 1U);
    t62 = (0 + t60);
    t36 = (t35 + t62);
    t38 = (t68 + 12U);
    t63 = *((unsigned int *)t38);
    t72 = (1U * t63);
    memcpy(t36, t32, t72);
    goto LAB13;

LAB15:    t7 = (t26 + 56U);
    t8 = *((char **)t7);
    t7 = (t24 + 0U);
    t27 = *((int *)t7);
    t10 = (t27 - 3);
    t15 = (t10 * 1U);
    t55 = (0 + t15);
    t12 = (t8 + t55);
    t16 = (t46 + 0U);
    t17 = (t16 + 0U);
    *((int *)t17) = 3;
    t17 = (t16 + 4U);
    *((int *)t17) = 0;
    t17 = (t16 + 8U);
    *((int *)t17) = -1;
    t49 = (0 - 3);
    t56 = (t49 * -1);
    t56 = (t56 + 1);
    t17 = (t16 + 12U);
    *((unsigned int *)t17) = t56;
    t17 = (t1 + 15622);
    t20 = (t66 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = 0;
    t21 = (t20 + 4U);
    *((int *)t21) = 3;
    t21 = (t20 + 8U);
    *((int *)t21) = 1;
    t54 = (3 - 0);
    t56 = (t54 * 1);
    t56 = (t56 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t56;
    t67 = ieee_std_logic_unsigned_greater_stdv_stdv(IEEE_P_3620187407, t12, t46, t17, t66);
    t41 = t67;
    goto LAB17;

LAB18:    t21 = (t26 + 56U);
    t22 = *((char **)t21);
    t21 = (t24 + 0U);
    t61 = *((int *)t21);
    t56 = (t61 - 7);
    t57 = (t56 * 1U);
    t58 = (0 + t57);
    t23 = (t22 + t58);
    t25 = (t69 + 0U);
    t28 = (t25 + 0U);
    *((int *)t28) = 7;
    t28 = (t25 + 4U);
    *((int *)t28) = 4;
    t28 = (t25 + 8U);
    *((int *)t28) = -1;
    t64 = (4 - 7);
    t59 = (t64 * -1);
    t59 = (t59 + 1);
    t28 = (t25 + 12U);
    *((unsigned int *)t28) = t59;
    t28 = (t1 + 15634);
    t31 = (t70 + 0U);
    t32 = (t31 + 0U);
    *((int *)t32) = 0;
    t32 = (t31 + 4U);
    *((int *)t32) = 3;
    t32 = (t31 + 8U);
    *((int *)t32) = 1;
    t65 = (3 - 0);
    t59 = (t65 * 1);
    t59 = (t59 + 1);
    t32 = (t31 + 12U);
    *((unsigned int *)t32) = t59;
    t32 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t68, t23, t69, t28, t70);
    t33 = (t26 + 56U);
    t35 = *((char **)t33);
    t33 = (t24 + 0U);
    t71 = *((int *)t33);
    t59 = (t71 - 7);
    t60 = (t59 * 1U);
    t62 = (0 + t60);
    t36 = (t35 + t62);
    t38 = (t68 + 12U);
    t63 = *((unsigned int *)t38);
    t72 = (1U * t63);
    memcpy(t36, t32, t72);
    goto LAB19;

LAB21:    t7 = (t26 + 56U);
    t8 = *((char **)t7);
    t7 = (t24 + 0U);
    t27 = *((int *)t7);
    t10 = (t27 - 7);
    t15 = (t10 * 1U);
    t55 = (0 + t15);
    t12 = (t8 + t55);
    t16 = (t46 + 0U);
    t17 = (t16 + 0U);
    *((int *)t17) = 7;
    t17 = (t16 + 4U);
    *((int *)t17) = 4;
    t17 = (t16 + 8U);
    *((int *)t17) = -1;
    t49 = (4 - 7);
    t56 = (t49 * -1);
    t56 = (t56 + 1);
    t17 = (t16 + 12U);
    *((unsigned int *)t17) = t56;
    t17 = (t1 + 15630);
    t20 = (t66 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = 0;
    t21 = (t20 + 4U);
    *((int *)t21) = 3;
    t21 = (t20 + 8U);
    *((int *)t21) = 1;
    t54 = (3 - 0);
    t56 = (t54 * 1);
    t56 = (t56 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t56;
    t67 = ieee_std_logic_unsigned_greater_stdv_stdv(IEEE_P_3620187407, t12, t46, t17, t66);
    t41 = t67;
    goto LAB23;

LAB25:    goto LAB5;

}

char *work_a_0519386517_3212880686_sub_1765416411_3057020925(char *t1, char *t2, char *t3)
{
    char t4[128];
    char t5[24];
    char t6[16];
    char t11[16];
    char t16[8];
    char *t0;
    char *t7;
    char *t8;
    int t9;
    unsigned int t10;
    char *t12;
    int t13;
    char *t14;
    char *t15;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    unsigned char t21;
    char *t22;
    char *t23;
    int t25;
    char *t26;
    int t28;
    char *t29;
    int t31;
    char *t32;
    int t34;
    char *t35;
    int t37;
    char *t38;
    int t40;
    char *t41;
    int t43;
    char *t44;
    int t46;
    char *t47;
    int t49;
    char *t50;
    int t52;
    char *t53;
    char *t55;
    char *t56;
    unsigned int t57;

LAB0:    t7 = (t6 + 0U);
    t8 = (t7 + 0U);
    *((int *)t8) = 3;
    t8 = (t7 + 4U);
    *((int *)t8) = 0;
    t8 = (t7 + 8U);
    *((int *)t8) = -1;
    t9 = (0 - 3);
    t10 = (t9 * -1);
    t10 = (t10 + 1);
    t8 = (t7 + 12U);
    *((unsigned int *)t8) = t10;
    t8 = (t11 + 0U);
    t12 = (t8 + 0U);
    *((int *)t12) = 6;
    t12 = (t8 + 4U);
    *((int *)t12) = 0;
    t12 = (t8 + 8U);
    *((int *)t12) = -1;
    t13 = (0 - 6);
    t10 = (t13 * -1);
    t10 = (t10 + 1);
    t12 = (t8 + 12U);
    *((unsigned int *)t12) = t10;
    t12 = (t4 + 4U);
    t14 = ((IEEE_P_2592010699) + 4024);
    t15 = (t12 + 88U);
    *((char **)t15) = t14;
    t17 = (t12 + 56U);
    *((char **)t17) = t16;
    xsi_type_set_default_value(t14, t16, t11);
    t18 = (t12 + 64U);
    *((char **)t18) = t11;
    t19 = (t12 + 80U);
    *((unsigned int *)t19) = 7U;
    t20 = (t5 + 4U);
    t21 = (t3 != 0);
    if (t21 == 1)
        goto LAB3;

LAB2:    t22 = (t5 + 12U);
    *((char **)t22) = t6;
    t23 = (t1 + 15638);
    t25 = xsi_mem_cmp(t23, t3, 4U);
    if (t25 == 1)
        goto LAB5;

LAB16:    t26 = (t1 + 15642);
    t28 = xsi_mem_cmp(t26, t3, 4U);
    if (t28 == 1)
        goto LAB6;

LAB17:    t29 = (t1 + 15646);
    t31 = xsi_mem_cmp(t29, t3, 4U);
    if (t31 == 1)
        goto LAB7;

LAB18:    t32 = (t1 + 15650);
    t34 = xsi_mem_cmp(t32, t3, 4U);
    if (t34 == 1)
        goto LAB8;

LAB19:    t35 = (t1 + 15654);
    t37 = xsi_mem_cmp(t35, t3, 4U);
    if (t37 == 1)
        goto LAB9;

LAB20:    t38 = (t1 + 15658);
    t40 = xsi_mem_cmp(t38, t3, 4U);
    if (t40 == 1)
        goto LAB10;

LAB21:    t41 = (t1 + 15662);
    t43 = xsi_mem_cmp(t41, t3, 4U);
    if (t43 == 1)
        goto LAB11;

LAB22:    t44 = (t1 + 15666);
    t46 = xsi_mem_cmp(t44, t3, 4U);
    if (t46 == 1)
        goto LAB12;

LAB23:    t47 = (t1 + 15670);
    t49 = xsi_mem_cmp(t47, t3, 4U);
    if (t49 == 1)
        goto LAB13;

LAB24:    t50 = (t1 + 15674);
    t52 = xsi_mem_cmp(t50, t3, 4U);
    if (t52 == 1)
        goto LAB14;

LAB25:
LAB15:    t7 = (t1 + 15748);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);

LAB4:    t7 = (t12 + 56U);
    t8 = *((char **)t7);
    t7 = (t11 + 12U);
    t10 = *((unsigned int *)t7);
    t10 = (t10 * 1U);
    t0 = xsi_get_transient_memory(t10);
    memcpy(t0, t8, t10);
    t14 = (t11 + 0U);
    t9 = *((int *)t14);
    t15 = (t11 + 4U);
    t13 = *((int *)t15);
    t17 = (t11 + 8U);
    t25 = *((int *)t17);
    t18 = (t2 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = t9;
    t19 = (t18 + 4U);
    *((int *)t19) = t13;
    t19 = (t18 + 8U);
    *((int *)t19) = t25;
    t28 = (t13 - t9);
    t57 = (t28 * t25);
    t57 = (t57 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t57;

LAB1:    return t0;
LAB3:    *((char **)t20) = t3;
    goto LAB2;

LAB5:    t53 = (t1 + 15678);
    t55 = (t12 + 56U);
    t56 = *((char **)t55);
    t55 = (t56 + 0);
    memcpy(t55, t53, 7U);
    goto LAB4;

LAB6:    t7 = (t1 + 15685);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB7:    t7 = (t1 + 15692);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB8:    t7 = (t1 + 15699);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB9:    t7 = (t1 + 15706);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB10:    t7 = (t1 + 15713);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB11:    t7 = (t1 + 15720);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB12:    t7 = (t1 + 15727);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB13:    t7 = (t1 + 15734);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB14:    t7 = (t1 + 15741);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB26:;
LAB27:;
}

static void work_a_0519386517_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(146, ng4);

LAB3:    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 9432);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 3U, 1, 0LL);

LAB2:    t8 = (t0 + 9192);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(147, ng4);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 9496);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 2U, 1, 0LL);

LAB2:    t8 = (t0 + 9208);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(148, ng4);

LAB3:    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 9560);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 1U, 1, 0LL);

LAB2:    t8 = (t0 + 9224);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(149, ng4);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 9624);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 0U, 1, 0LL);

LAB2:    t8 = (t0 + 9240);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(184, ng4);

LAB3:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 9688);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 3U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9256);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_5(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(185, ng4);

LAB3:    t1 = (t0 + 4392U);
    t2 = *((char **)t1);
    t1 = (t0 + 9752);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 8U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9272);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_6(char *t0)
{
    char t5[16];
    char t17[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned char t21;
    unsigned char t22;

LAB0:    xsi_set_current_line(193, ng4);
    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 15268U);
    t3 = (t0 + 15755);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 15268U);
    t3 = (t0 + 15786);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB5;

LAB6:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 15268U);
    t3 = (t0 + 15817);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB7;

LAB8:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 15268U);
    t3 = (t0 + 15820);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB17;

LAB18:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 15268U);
    t3 = (t0 + 15851);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB19;

LAB20:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 15268U);
    t3 = (t0 + 15854);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB32;

LAB33:
LAB3:    t1 = (t0 + 9288);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(195, ng4);
    t7 = (t0 + 15758);
    t12 = (t0 + 9816);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 7U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(196, ng4);
    t1 = (t0 + 15765);
    t3 = (t0 + 9880);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(197, ng4);
    t1 = (t0 + 15772);
    t3 = (t0 + 9944);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(198, ng4);
    t1 = (t0 + 15779);
    t3 = (t0 + 10008);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB3;

LAB5:    xsi_set_current_line(202, ng4);
    t7 = (t0 + 15789);
    t12 = (t0 + 9816);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 7U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(203, ng4);
    t1 = (t0 + 15796);
    t3 = (t0 + 9880);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(204, ng4);
    t1 = (t0 + 15803);
    t3 = (t0 + 9944);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(205, ng4);
    t1 = (t0 + 15810);
    t3 = (t0 + 10008);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB3;

LAB7:    xsi_set_current_line(212, ng4);
    t7 = (t0 + 4392U);
    t11 = *((char **)t7);
    t7 = work_a_0519386517_3212880686_sub_4141168059_3057020925(t0, t17, t11);
    t12 = (t0 + 5288U);
    t13 = *((char **)t12);
    t12 = (t13 + 0);
    t14 = (t17 + 12U);
    t9 = *((unsigned int *)t14);
    t9 = (t9 * 1U);
    memcpy(t12, t7, t9);
    xsi_set_current_line(218, ng4);
    t1 = (t0 + 5288U);
    t2 = *((char **)t1);
    t9 = (7 - 3);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t2 + t19);
    t3 = work_a_0519386517_3212880686_sub_1765416411_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t20 = *((unsigned int *)t4);
    t20 = (t20 * 1U);
    t10 = (7U != t20);
    if (t10 == 1)
        goto LAB9;

LAB10:    t6 = (t0 + 10008);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(219, ng4);
    t1 = (t0 + 5288U);
    t2 = *((char **)t1);
    t9 = (7 - 7);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t2 + t19);
    t3 = work_a_0519386517_3212880686_sub_1765416411_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t20 = *((unsigned int *)t4);
    t20 = (t20 * 1U);
    t10 = (7U != t20);
    if (t10 == 1)
        goto LAB11;

LAB12:    t6 = (t0 + 9944);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(220, ng4);
    t1 = (t0 + 3912U);
    t2 = *((char **)t1);
    t1 = (t0 + 5168U);
    t3 = *((char **)t1);
    t9 = (7 - 3);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t3 + t19);
    memcpy(t1, t2, 4U);
    xsi_set_current_line(221, ng4);
    t1 = (t0 + 5168U);
    t2 = *((char **)t1);
    t1 = work_a_0519386517_3212880686_sub_4141168059_3057020925(t0, t5, t2);
    t3 = (t0 + 5408U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    t6 = (t5 + 12U);
    t9 = *((unsigned int *)t6);
    t9 = (t9 * 1U);
    memcpy(t3, t1, t9);
    xsi_set_current_line(222, ng4);
    t1 = (t0 + 5408U);
    t2 = *((char **)t1);
    t9 = (7 - 3);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t2 + t19);
    t3 = work_a_0519386517_3212880686_sub_1765416411_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t20 = *((unsigned int *)t4);
    t20 = (t20 * 1U);
    t10 = (7U != t20);
    if (t10 == 1)
        goto LAB13;

LAB14:    t6 = (t0 + 9880);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(223, ng4);
    t1 = (t0 + 5408U);
    t2 = *((char **)t1);
    t9 = (7 - 7);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t2 + t19);
    t3 = work_a_0519386517_3212880686_sub_1765416411_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t20 = *((unsigned int *)t4);
    t20 = (t20 * 1U);
    t10 = (7U != t20);
    if (t10 == 1)
        goto LAB15;

LAB16:    t6 = (t0 + 9816);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    goto LAB3;

LAB9:    xsi_size_not_matching(7U, t20, 0);
    goto LAB10;

LAB11:    xsi_size_not_matching(7U, t20, 0);
    goto LAB12;

LAB13:    xsi_size_not_matching(7U, t20, 0);
    goto LAB14;

LAB15:    xsi_size_not_matching(7U, t20, 0);
    goto LAB16;

LAB17:    xsi_set_current_line(230, ng4);
    t7 = (t0 + 15823);
    t12 = (t0 + 9816);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 7U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(231, ng4);
    t1 = (t0 + 15830);
    t3 = (t0 + 9880);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(232, ng4);
    t1 = (t0 + 15837);
    t3 = (t0 + 9944);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(233, ng4);
    t1 = (t0 + 15844);
    t3 = (t0 + 10008);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB3;

LAB19:    xsi_set_current_line(237, ng4);
    t7 = (t0 + 1032U);
    t11 = *((char **)t7);
    t21 = *((unsigned char *)t11);
    t22 = (t21 == (unsigned char)3);
    if (t22 != 0)
        goto LAB21;

LAB23:
LAB22:    goto LAB3;

LAB21:    xsi_set_current_line(241, ng4);
    t7 = (t0 + 4072U);
    t12 = *((char **)t7);
    t7 = (t0 + 5168U);
    t13 = *((char **)t7);
    t9 = (7 - 3);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t7 = (t13 + t19);
    memcpy(t7, t12, 4U);
    xsi_set_current_line(242, ng4);
    t1 = (t0 + 5168U);
    t2 = *((char **)t1);
    t1 = work_a_0519386517_3212880686_sub_4141168059_3057020925(t0, t5, t2);
    t3 = (t0 + 5288U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    t6 = (t5 + 12U);
    t9 = *((unsigned int *)t6);
    t9 = (t9 * 1U);
    memcpy(t3, t1, t9);
    xsi_set_current_line(245, ng4);
    t1 = (t0 + 5288U);
    t2 = *((char **)t1);
    t9 = (7 - 3);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t2 + t19);
    t3 = work_a_0519386517_3212880686_sub_1765416411_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t20 = *((unsigned int *)t4);
    t20 = (t20 * 1U);
    t10 = (7U != t20);
    if (t10 == 1)
        goto LAB24;

LAB25:    t6 = (t0 + 10008);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(246, ng4);
    t1 = (t0 + 5288U);
    t2 = *((char **)t1);
    t9 = (7 - 7);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t2 + t19);
    t3 = work_a_0519386517_3212880686_sub_1765416411_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t20 = *((unsigned int *)t4);
    t20 = (t20 * 1U);
    t10 = (7U != t20);
    if (t10 == 1)
        goto LAB26;

LAB27:    t6 = (t0 + 9944);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(247, ng4);
    t1 = (t0 + 3912U);
    t2 = *((char **)t1);
    t1 = (t0 + 5168U);
    t3 = *((char **)t1);
    t9 = (7 - 3);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t3 + t19);
    memcpy(t1, t2, 4U);
    xsi_set_current_line(248, ng4);
    t1 = (t0 + 5168U);
    t2 = *((char **)t1);
    t1 = work_a_0519386517_3212880686_sub_4141168059_3057020925(t0, t5, t2);
    t3 = (t0 + 5408U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    t6 = (t5 + 12U);
    t9 = *((unsigned int *)t6);
    t9 = (t9 * 1U);
    memcpy(t3, t1, t9);
    xsi_set_current_line(249, ng4);
    t1 = (t0 + 5408U);
    t2 = *((char **)t1);
    t9 = (7 - 3);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t2 + t19);
    t3 = work_a_0519386517_3212880686_sub_1765416411_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t20 = *((unsigned int *)t4);
    t20 = (t20 * 1U);
    t10 = (7U != t20);
    if (t10 == 1)
        goto LAB28;

LAB29:    t6 = (t0 + 9880);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(250, ng4);
    t1 = (t0 + 5408U);
    t2 = *((char **)t1);
    t9 = (7 - 7);
    t18 = (t9 * 1U);
    t19 = (0 + t18);
    t1 = (t2 + t19);
    t3 = work_a_0519386517_3212880686_sub_1765416411_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t20 = *((unsigned int *)t4);
    t20 = (t20 * 1U);
    t10 = (7U != t20);
    if (t10 == 1)
        goto LAB30;

LAB31:    t6 = (t0 + 9816);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    goto LAB22;

LAB24:    xsi_size_not_matching(7U, t20, 0);
    goto LAB25;

LAB26:    xsi_size_not_matching(7U, t20, 0);
    goto LAB27;

LAB28:    xsi_size_not_matching(7U, t20, 0);
    goto LAB29;

LAB30:    xsi_size_not_matching(7U, t20, 0);
    goto LAB31;

LAB32:    xsi_set_current_line(280, ng4);
    t7 = (t0 + 15857);
    t12 = (t0 + 9816);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 7U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(281, ng4);
    t1 = (t0 + 15864);
    t3 = (t0 + 9880);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(282, ng4);
    t1 = (t0 + 15871);
    t3 = (t0 + 9944);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(283, ng4);
    t1 = (t0 + 15878);
    t3 = (t0 + 10008);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB3;

}

static void work_a_0519386517_3212880686_p_7(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(288, ng4);

LAB3:    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 10072);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 7U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9304);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_8(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(289, ng4);

LAB3:    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 10136);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 7U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9320);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_9(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(290, ng4);

LAB3:    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t1 = (t0 + 10200);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 7U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9336);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_10(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(291, ng4);

LAB3:    t1 = (t0 + 3752U);
    t2 = *((char **)t1);
    t1 = (t0 + 10264);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 7U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9352);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_0519386517_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0519386517_3212880686_p_0,(void *)work_a_0519386517_3212880686_p_1,(void *)work_a_0519386517_3212880686_p_2,(void *)work_a_0519386517_3212880686_p_3,(void *)work_a_0519386517_3212880686_p_4,(void *)work_a_0519386517_3212880686_p_5,(void *)work_a_0519386517_3212880686_p_6,(void *)work_a_0519386517_3212880686_p_7,(void *)work_a_0519386517_3212880686_p_8,(void *)work_a_0519386517_3212880686_p_9,(void *)work_a_0519386517_3212880686_p_10};
	static char *se[] = {(void *)work_a_0519386517_3212880686_sub_4141168059_3057020925,(void *)work_a_0519386517_3212880686_sub_1765416411_3057020925};
	xsi_register_didat("work_a_0519386517_3212880686", "isim/top_tb_isim_beh.exe.sim/work/a_0519386517_3212880686.didat");
	xsi_register_executes(pe);
	xsi_register_subprogram_executes(se);
}
