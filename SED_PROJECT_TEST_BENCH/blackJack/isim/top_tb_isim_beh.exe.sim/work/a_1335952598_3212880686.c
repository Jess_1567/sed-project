/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/.Xilinx/Bueno para Test-Bench/blackJack/top.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_1335952598_3212880686_p_0(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned char t6;
    unsigned int t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(130, ng0);
    t1 = (t0 + 1472U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 5480);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(131, ng0);
    t3 = (t0 + 2792U);
    t4 = *((char **)t3);
    t3 = (t0 + 9270);
    t6 = 1;
    if (4U == 4U)
        goto LAB8;

LAB9:    t6 = 0;

LAB10:    if (t6 != 0)
        goto LAB5;

LAB7:    t1 = (t0 + 2792U);
    t3 = *((char **)t1);
    t1 = (t0 + 9274);
    t2 = 1;
    if (4U == 4U)
        goto LAB16;

LAB17:    t2 = 0;

LAB18:    if (t2 != 0)
        goto LAB14;

LAB15:    t1 = (t0 + 2792U);
    t3 = *((char **)t1);
    t1 = (t0 + 9278);
    t2 = 1;
    if (4U == 4U)
        goto LAB24;

LAB25:    t2 = 0;

LAB26:    if (t2 != 0)
        goto LAB22;

LAB23:    t1 = (t0 + 2792U);
    t3 = *((char **)t1);
    t1 = (t0 + 9282);
    t2 = 1;
    if (4U == 4U)
        goto LAB32;

LAB33:    t2 = 0;

LAB34:    if (t2 != 0)
        goto LAB30;

LAB31:
LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(132, ng0);
    t10 = (t0 + 2952U);
    t11 = *((char **)t10);
    t10 = (t0 + 5576);
    t12 = (t10 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t11, 7U);
    xsi_driver_first_trans_fast_port(t10);
    goto LAB6;

LAB8:    t7 = 0;

LAB11:    if (t7 < 4U)
        goto LAB12;
    else
        goto LAB10;

LAB12:    t8 = (t4 + t7);
    t9 = (t3 + t7);
    if (*((unsigned char *)t8) != *((unsigned char *)t9))
        goto LAB9;

LAB13:    t7 = (t7 + 1);
    goto LAB11;

LAB14:    xsi_set_current_line(138, ng0);
    t9 = (t0 + 3112U);
    t10 = *((char **)t9);
    t9 = (t0 + 5640);
    t11 = (t9 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t10, 7U);
    xsi_driver_first_trans_fast_port(t9);
    goto LAB6;

LAB16:    t7 = 0;

LAB19:    if (t7 < 4U)
        goto LAB20;
    else
        goto LAB18;

LAB20:    t5 = (t3 + t7);
    t8 = (t1 + t7);
    if (*((unsigned char *)t5) != *((unsigned char *)t8))
        goto LAB17;

LAB21:    t7 = (t7 + 1);
    goto LAB19;

LAB22:    xsi_set_current_line(144, ng0);
    t9 = (t0 + 3272U);
    t10 = *((char **)t9);
    t9 = (t0 + 5704);
    t11 = (t9 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t10, 7U);
    xsi_driver_first_trans_fast_port(t9);
    goto LAB6;

LAB24:    t7 = 0;

LAB27:    if (t7 < 4U)
        goto LAB28;
    else
        goto LAB26;

LAB28:    t5 = (t3 + t7);
    t8 = (t1 + t7);
    if (*((unsigned char *)t5) != *((unsigned char *)t8))
        goto LAB25;

LAB29:    t7 = (t7 + 1);
    goto LAB27;

LAB30:    xsi_set_current_line(150, ng0);
    t9 = (t0 + 3432U);
    t10 = *((char **)t9);
    t9 = (t0 + 5768);
    t11 = (t9 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t10, 7U);
    xsi_driver_first_trans_fast_port(t9);
    goto LAB6;

LAB32:    t7 = 0;

LAB35:    if (t7 < 4U)
        goto LAB36;
    else
        goto LAB34;

LAB36:    t5 = (t3 + t7);
    t8 = (t1 + t7);
    if (*((unsigned char *)t5) != *((unsigned char *)t8))
        goto LAB33;

LAB37:    t7 = (t7 + 1);
    goto LAB35;

}

static void work_a_1335952598_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(163, ng0);

LAB3:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 5832);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 4U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 5496);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_1335952598_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1335952598_3212880686_p_0,(void *)work_a_1335952598_3212880686_p_1};
	xsi_register_didat("work_a_1335952598_3212880686", "isim/top_tb_isim_beh.exe.sim/work/a_1335952598_3212880686.didat");
	xsi_register_executes(pe);
}
