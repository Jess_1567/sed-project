----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:15:40 01/04/2016 
-- Design Name: 
-- Module Name:    binTobcd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all ;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity binTobcd is
    Port ( bin : in  STD_LOGIC_VECTOR (7 downto 0);
           bcd : out  STD_LOGIC_VECTOR (7 downto 0));
end binTobcd;

architecture Behavioral of binTobcd is
begin
	function to_bcd(bin: std_logic_vector(7 downto 0)) return std_logic_vector is
		variable bin_temp : STD_LOGIC_VECTOR (7 downto 0):=bin;
		variable bcd : STD_LOGIC_VECTOR (7 downto 0):=(others=>'0');
		variable i : integer:=0;
	begin
		for i in 0 to 7 loop 
			bcd(7 downto 1) := bcd(6 downto 0); --shift left 
			bcd(0) := bin(7); --shift left
			bin_temp(7 downto 1) := bin_temp(6 downto 0);
			bin_temp(0) :='0';
			if  (i < 7 and bcd(3 downto 0) > "0100") then 
				bcd(3 downto 0) := bcd(3 downto 0) + "0011"; --add 3
			end if;
			if   (i < 7 and bcd(7 downto 4) > "0100")  then 
				bcd(7 downto 4) := bcd(7 downto 4) + "0011"; --add 3
			end if;
		end loop;
		return bcd;
	end to_bcd;
end Behavioral;

