
-- VHDL Instantiation Created from source file Sumador.vhd -- 20:11:29 01/21/2016
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT Sumador
	PORT(
		play : IN std_logic;
		over : IN std_logic;
		estado : IN std_logic_vector(2 downto 0);
		clk : IN std_logic;
		R : IN std_logic;
		sumando : IN std_logic_vector(3 downto 0);
		valor_previo : IN std_logic_vector(7 downto 0);          
		suma : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;

	Inst_Sumador: Sumador PORT MAP(
		play => ,
		over => ,
		estado => ,
		clk => ,
		R => ,
		sumando => ,
		valor_previo => ,
		suma => 
	);


