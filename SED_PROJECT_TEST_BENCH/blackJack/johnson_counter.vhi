
-- VHDL Instantiation Created from source file johnson_counter.vhd -- 17:50:06 01/21/2016
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT johnson_counter
	PORT(
		R : IN std_logic;
		clk : IN std_logic;          
		data : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

	Inst_johnson_counter: johnson_counter PORT MAP(
		R => ,
		clk => ,
		data => 
	);


