----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:37:06 01/14/2016 
-- Design Name: 
-- Module Name:    divisor - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--

library IEEE;

use IEEE.STD_LOGIC_1164.ALL;



entity divisor is

    Port ( R : in  STD_LOGIC;
           clk_in : in  STD_LOGIC;
           clk_out : out  STD_LOGIC);
end divisor;


architecture Behavioral of divisor is
signal temp: std_logic;
signal counter: integer range 0 to 12499;

begin

    clk_out <=temp;
    freq_d : process(R, clk_in)
    begin
        if rising_edge(clk_in) then
           if R='1' then    
                temp <= '0';
                counter <= 0;

            else 
                if counter = 1249 then 
                    temp <= not (temp);
                   counter <= 0;

                else
                    counter <= counter + 1;
                end if;
            end if;
        end if;
    end process;
end Behavioral;

