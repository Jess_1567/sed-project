
-- VHDL Instantiation Created from source file divisor.vhd -- 18:34:47 01/21/2016
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT divisor
	PORT(
		R : IN std_logic;
		clk_in : IN std_logic;          
		clk_out : OUT std_logic
		);
	END COMPONENT;

	Inst_divisor: divisor PORT MAP(
		R => ,
		clk_in => ,
		clk_out => 
	);


