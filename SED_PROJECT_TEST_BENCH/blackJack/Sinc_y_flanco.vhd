----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:34:51 01/21/2016 
-- Design Name: 
-- Module Name:    Sinc_y_flanco - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Sinc_y_flanco is
    Port ( pulsador 			: in  STD_LOGIC;
			  clk					: in STD_LOGIC; 
			  reset				: in STD_LOGIC; 
           flanco_positivo : out  STD_LOGIC);
end Sinc_y_flanco;


architecture Behavioral of Sinc_y_flanco is

----SINCRONIZADOR
signal sreg: std_logic_vector(1 downto 0); 
attribute ASYNC_REG: string; 
attribute ASYNC_REG of sreg: signal is "TRUE"; 

----DETECTOR DE FLANCO
signal Q1, Q2, Q3: std_logic; 

signal temp: std_logic; 
	
begin
	
	--SINCRONIZADOR
	process (clk) 
	begin
	--	if clk_temp'event and clk_temp = '1' then
		if rising_edge (clk) then
			temp <= sreg(1); 
			sreg <= sreg(0) & pulsador; 
		end if; 
	end process; 
	
	--DETECTOR DE FLANCOS
	process (clk)
	begin
		if reset = '1' then
			Q1 <= '0'; 
			Q2 <= '0'; 
			Q3 <= '0'; 
			
		elsif (clk'event and clk = '1') then
			Q1 <= temp; 
			Q2 <= Q1; 
			Q3 <= Q2; 
		end if; 
	end process; 
	flanco_positivo <= Q1 and Q2 and (not Q3); 
	
end Behavioral;