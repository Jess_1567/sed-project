----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:45:08 12/26/2015 
-- Design Name: 
-- Module Name:    lfsr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lfsr is
    Port ( clk : in  STD_LOGIC;
           R : in  STD_LOGIC;
           data : out  STD_LOGIC_VECTOR (15 downto 0));
end lfsr;

architecture Behavioral of lfsr is
	signal temp : std_logic_vector(15 downto 0) :="0110100011110011";
begin
	data <= temp;
	
	process(clk,R)
	variable value1 : std_logic;
	variable value2 : std_logic;
	variable value3 : std_logic;
	begin
		if rising_edge(clk) then
			if R = '1' then
				temp <= "0110100011110011";
			else
				value1 := (temp(2) xor temp(0));
				value2 := (temp(3) xor value1);
				value3 := (temp(5) xor value2);
				temp(14 downto 0) <=temp(15 downto 1);
				temp(15) <= (value3);
			end if;
		end if;
	end process;
end Behavioral;

