----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:20:33 01/01/2016 
-- Design Name: 
-- Module Name:    blackJack_game - Behavioral 
-- Project Name: Blackjack simplificado para FPGA
-- Target Devices: 
-- Tool versions: 
-- Description: en este modulo se realiza la dinamica de juego 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: las cartas se representaran en el display de la siguiente forma:
-- A-1, 2-2,3-3,4-4, 5-5,6-6,7-7,8-8,9-9,10-10, J-11,Q-12,K-13
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all ;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blackJack_game is
    Port ( 	play      : IN  std_logic; --Boton de accion
				R         : IN  std_logic; --Reset
				plantarse : IN  std_logic; --Boton de plantarse
				clk       : IN  std_logic; --Senal de reloj        
				display1  : OUT std_logic_vector(6 downto 0); --Senales que controlan que 
				display2  : OUT std_logic_vector(6 downto 0); --poner en cada display en 
				display3  : OUT std_logic_vector(6 downto 0); --cada momento
				display4  : OUT std_logic_vector(6 downto 0)  --
			 );
end blackJack_game;

architecture Behavioral of blackJack_game is
COMPONENT blackJack --genera las cartas
	PORT(
		play  : IN  std_logic; --Boton de accion
		R     : IN  std_logic; --Reset
		clk   : IN  std_logic; --Senal de reloj          
		carta : out STD_LOGIC_VECTOR (3 downto 0); --Registro que lleva la informacion de la carta generada
		valor : out STD_LOGIC_VECTOR (3 downto 0)  --Registro que nos indica el valor de la carta generada para el juego
		);
	END COMPONENT;
	
COMPONENT FSM --estados por los que pasa el juego
	PORT(
		inputs : IN  std_logic_vector(3 downto 0); --Registro que lleva la informacion de las acciones a realizar
		--------------------------------------------------------------------
		--inputs(0) over2     --Gestiona cuando se pasa de 21 el jugador 2--
		--inputs(1) over1     --Gestiona cuando se pasa de 21 el jugador 2--
		--inputs(2) plantarse --Boton de plantarse                        --
		--inputs(3) play      --Boton de accion                           --
		--------------------------------------------------------------------
		R      : IN  std_logic; --Reset
		clk    : IN  std_logic; --Senal de reloj         
		estado : OUT std_logic_vector(2 downto 0) --Salida que nos permite saber en que estado estamos en cada instante
		);
	END COMPONENT;

	COMPONENT SumadorJug1
	PORT(
		play    : IN  std_logic; --Boton de accion
		estado  : IN  std_logic_vector(2 downto 0); --Senal que lleva la informacion de la maquina de estados global
		clk     : IN  std_logic; --Senal de reloj
		R       : IN  std_logic; --Reset
		sumando : IN  std_logic_vector(3 downto 0); --Valor de la carta que se desea sumar a lo acumulado
		suma    : OUT std_logic_vector(7 downto 0)  --Valor obtenido de la suma entre lo acumulado y el valor pasado por el sumando
		);
	END COMPONENT;
	
	COMPONENT SumadorJug2
	PORT(
		play    : IN  std_logic; --Boton de accion
		estado  : IN  std_logic_vector(2 downto 0); --Senal que lleva la informacion de la maquina de estados global
		clk     : IN  std_logic; --Senal de reloj
		R       : IN  std_logic; --Reset
		sumando : IN  std_logic_vector(3 downto 0); --Valor de la carta que se desea sumar a lo acumulado
		suma    : OUT std_logic_vector(7 downto 0)  --Valor obtenido de la suma entre lo acumulado y el valor pasado por el sumando
		);
	END COMPONENT;

--Funcion que nos permite pasar de binario a bcd. Es necesaria para la representacion en los display	
function to_bcd(bin: std_logic_vector(7 downto 0)) return std_logic_vector is
	variable bin_temp : STD_LOGIC_VECTOR (7 downto 0) := bin;
	variable bcd      : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
	variable i        : integer:=0;
begin
	for i in 0 to 7 loop 
		bcd(7 downto 1) := bcd(6 downto 0); --shift left 
		bcd(0) := bin_temp(7); --shift left
		bin_temp(7 downto 1) := bin_temp(6 downto 0);
		bin_temp(0) :='0';
		if  (i < 7 and bcd(3 downto 0) > "0100") then 
			bcd(3 downto 0) := bcd(3 downto 0) + "0011"; --add 3
		end if;
		if   (i < 7 and bcd(7 downto 4) > "0100")  then 
			bcd(7 downto 4) := bcd(7 downto 4) + "0011"; --add 3
		end if;
	end loop;
   return bcd;
end to_bcd;

--Funcion que nos permite tener un decodificador necesario para la representacion en los displays
function decoder(code: std_logic_vector(3 downto 0)) return std_logic_vector is
	variable led : std_logic_vector(6 downto 0);
	begin
	case code is 
		WHEN "0000" => led := "0000001";
		WHEN "0001" => led := "1001111";
		WHEN "0010" => led := "0010010";
		WHEN "0011" => led := "0000110";
		WHEN "0100" => led := "1001100";
		WHEN "0101" => led := "0100100";
		WHEN "0110" => led := "0100000";
		WHEN "0111" => led := "0001111";
		WHEN "1000" => led := "0000000";
		WHEN "1001" => led := "0000100"; 
		when others => led := "1111110";
	end case;
	return led;
end decoder;

	signal over1        : std_logic := '0';
	signal over2        : std_logic := '0';
	signal input_vector : std_logic_vector (3 downto 0);
	signal est_vector   : std_logic_vector (2 downto 0) := "001"; --vector de estados
	signal temp1        : std_logic_vector (6 downto 0);
	signal temp2        : std_logic_vector (6 downto 0);
	signal temp3        : std_logic_vector (6 downto 0);
	signal temp4        : std_logic_vector (6 downto 0);
	signal carta_BJ     : std_logic_vector (3 downto 0);
	signal valor_BJ     : std_logic_vector (3 downto 0);

	signal suma_total_1 : std_logic_vector (7 downto 0) := (others => '0'); 
	signal suma_total_2 : std_logic_vector (7 downto 0) := (others => '0'); 
	
begin
	input_vector(0) <= over2;
	input_vector(1) <= over1;
	input_vector(2) <= plantarse;
	input_vector(3) <= play;
	
	Inst_BlackJack: blackJack PORT MAP(
		play  => play ,
		R     => R,
		clk   => clk,
		carta => carta_BJ,
		valor => valor_BJ --valor BlackJack
	);
	
	Inst_FSM: FSM PORT MAP(
		inputs => input_vector ,
		R      => R,
		clk    => clk ,
		estado => est_vector
	);

	Inst_SumadorJug1: SumadorJug1 PORT MAP(
		play    => play,
		estado  => est_vector,
		clk     => clk,
		R       => R,
		sumando => valor_BJ,
		suma    => suma_total_1
	);
	
	Inst_SumadorJug2: SumadorJug2 PORT MAP(
		play    => play,
		estado  => est_vector,
		clk     => clk,
		R       => R,
		sumando => valor_BJ,
		suma    => suma_total_2
	);
	
	
	process(input_vector, clk)
	
	variable bin1     : std_logic_vector(7 downto 0) := (others => '0');
	variable bcd1     : std_logic_vector(7 downto 0);
	variable bcd2     : std_logic_vector(7 downto 0);
	variable over1var : std_logic;
	variable over2var : std_logic;
	
	begin
		if est_vector = "001" then --estado1
			--escribimos play 
			temp1 <= "0011000";
			temp2 <= "1110001";
			temp3 <= "0001000"; 
			temp4 <= "1000100";
			
		elsif est_vector = "010" then  --estado2
			--escribimos jug1
			temp1 <= "1000111";
			temp2 <= "1000001";
			temp3 <= "0100000";
			temp4 <= "1001111";
			
		elsif est_vector = "011" then --estado3
			
			bin1(3 downto 0):= carta_BJ;
			bcd2 := to_bcd(bin1);
			temp2 <= decoder(bcd2(3 downto 0));
			temp1 <= decoder(bcd2(7 downto 4)); 
			bcd1 := to_bcd(suma_total_1);
			temp4 <= decoder(bcd1(3 downto 0));
			temp3 <= decoder(bcd1(7 downto 4));
					
			if suma_total_1 > "00010101" then							
				over1   <= '1';
				over1var:= '1';
			end if;
					
			if suma_total_1 = "00000000" then 
				temp1 <= "0000001";
				temp2 <= "0000001";
				temp3 <= "0000001";
				temp4 <= "0000001";
			end if;
			
		
		elsif est_vector = "100" then --estado4
			--escribimos jug2
			temp1 <= "1000111";
			temp2 <= "1000001";
			temp3 <= "0100000";
			temp4 <= "0010010";
			

		elsif est_vector = "101" then --estado5
		
			bin1(3 downto 0):= carta_BJ;
			bcd2 := to_bcd(bin1);
			temp2 <= decoder(bcd2(3 downto 0));
			temp1 <= decoder(bcd2(7 downto 4));
			
			bcd1 := to_bcd(suma_total_2);			
			temp4 <= decoder(bcd1(3 downto 0));
			temp3 <= decoder(bcd1(7 downto 4)); 
			 
			if suma_total_2 > "00010101" then
				over2 <= '1';
				over2var:= '1';
			end if;
			
			if suma_total_2 = "00000000" then 
				temp1 <= "0000001";
				temp2 <= "0000001";
				temp3 <= "0000001";
				temp4 <= "0000001";
			end if;


		elsif est_vector = "110" then --estado6
		
			if (suma_total_1 = suma_total_2) or (suma_total_1 > "00010101" and suma_total_2 > "00010101") then  --empate
				--escribimos 4 rayas
				temp1 <= "1111110";
				temp2 <= "1111110";
				temp3 <= "1111110";
				temp4 <= "1111110";
				
			elsif ((suma_total_1 > suma_total_2 and suma_total_1 < "00010101") or ((suma_total_1 < "00010101") and (suma_total_2 > "00010101"))) then --gana jug1
				temp1 <= "1000111";
				temp2 <= "1000001";
				temp3 <= "0100000";
				temp4 <= "1001111";
				
			else --gana jug2
				--escribimos jug2
				temp1 <= "1000111";
				temp2 <= "1000001";
				temp3 <= "0100000";
				temp4 <= "0010010";
				
			end if;

		end if;
		
	end process;
	
	display1 <= temp1;
	display2 <= temp2;
	display3 <= temp3;
	display4 <= temp4;

end Behavioral;

