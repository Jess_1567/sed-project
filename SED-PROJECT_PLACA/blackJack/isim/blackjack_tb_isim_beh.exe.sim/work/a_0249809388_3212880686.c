/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/daaguirre/blackJack/blackJack_cartas.vhd";
extern char *IEEE_P_1242562249;
extern char *IEEE_P_2592010699;

int ieee_p_1242562249_sub_17802405650254020620_1035706684(char *, char *, char *);
unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_0249809388_3212880686_p_0(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    unsigned char t7;
    unsigned char t8;
    char *t9;
    int t10;
    char *t11;
    char *t12;
    int t13;

LAB0:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 1032U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:    t2 = (t0 + 1312U);
    t4 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t2, 0U, 0U);
    if (t4 == 1)
        goto LAB10;

LAB11:    t1 = (unsigned char)0;

LAB12:    if (t1 != 0)
        goto LAB8;

LAB9:
LAB3:    xsi_set_current_line(77, ng0);
    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t1 = (t10 <= 5040);
    if (t1 != 0)
        goto LAB13;

LAB15:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 5040);
    if (t4 == 1)
        goto LAB18;

LAB19:    t1 = (unsigned char)0;

LAB20:    if (t1 != 0)
        goto LAB16;

LAB17:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 10081);
    if (t4 == 1)
        goto LAB23;

LAB24:    t1 = (unsigned char)0;

LAB25:    if (t1 != 0)
        goto LAB21;

LAB22:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 15122);
    if (t4 == 1)
        goto LAB28;

LAB29:    t1 = (unsigned char)0;

LAB30:    if (t1 != 0)
        goto LAB26;

LAB27:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 20163);
    if (t4 == 1)
        goto LAB33;

LAB34:    t1 = (unsigned char)0;

LAB35:    if (t1 != 0)
        goto LAB31;

LAB32:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 25204);
    if (t4 == 1)
        goto LAB38;

LAB39:    t1 = (unsigned char)0;

LAB40:    if (t1 != 0)
        goto LAB36;

LAB37:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 30245);
    if (t4 == 1)
        goto LAB43;

LAB44:    t1 = (unsigned char)0;

LAB45:    if (t1 != 0)
        goto LAB41;

LAB42:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 35286);
    if (t4 == 1)
        goto LAB48;

LAB49:    t1 = (unsigned char)0;

LAB50:    if (t1 != 0)
        goto LAB46;

LAB47:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 40327);
    if (t4 == 1)
        goto LAB53;

LAB54:    t1 = (unsigned char)0;

LAB55:    if (t1 != 0)
        goto LAB51;

LAB52:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 45368);
    if (t4 == 1)
        goto LAB58;

LAB59:    t1 = (unsigned char)0;

LAB60:    if (t1 != 0)
        goto LAB56;

LAB57:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 50409);
    if (t4 == 1)
        goto LAB63;

LAB64:    t1 = (unsigned char)0;

LAB65:    if (t1 != 0)
        goto LAB61;

LAB62:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t4 = (t10 > 55450);
    if (t4 == 1)
        goto LAB68;

LAB69:    t1 = (unsigned char)0;

LAB70:    if (t1 != 0)
        goto LAB66;

LAB67:    t2 = (t0 + 2128U);
    t3 = *((char **)t2);
    t10 = *((int *)t3);
    t1 = (t10 == 65536);
    if (t1 != 0)
        goto LAB71;

LAB72:    xsi_set_current_line(117, ng0);
    t2 = (t0 + 6192);
    t6 = (t0 + 2248U);
    t9 = *((char **)t6);
    t6 = (t9 + 0);
    memcpy(t6, t2, 4U);
    xsi_set_current_line(118, ng0);
    t2 = (t0 + 6196);
    t6 = (t0 + 2368U);
    t9 = *((char **)t6);
    t6 = (t9 + 0);
    memcpy(t6, t2, 4U);

LAB14:    xsi_set_current_line(120, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 3744);
    t6 = (t2 + 56U);
    t9 = *((char **)t6);
    t11 = (t9 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t3, 4U);
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(121, ng0);
    t2 = (t0 + 2368U);
    t3 = *((char **)t2);
    t2 = (t0 + 3808);
    t6 = (t2 + 56U);
    t9 = *((char **)t6);
    t11 = (t9 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t3, 4U);
    xsi_driver_first_trans_fast_port(t2);
    t2 = (t0 + 3664);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(71, ng0);
    t2 = (t0 + 1832U);
    t9 = *((char **)t2);
    t2 = (t0 + 6024U);
    t10 = ieee_p_1242562249_sub_17802405650254020620_1035706684(IEEE_P_1242562249, t9, t2);
    t11 = (t0 + 2128U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    *((int *)t11) = t10;
    goto LAB3;

LAB5:    t2 = (t0 + 1192U);
    t6 = *((char **)t2);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)2);
    t1 = t8;
    goto LAB7;

LAB8:    xsi_set_current_line(73, ng0);
    t3 = (t0 + 6128);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t3, 4U);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB3;

LAB10:    t3 = (t0 + 1192U);
    t6 = *((char **)t3);
    t5 = *((unsigned char *)t6);
    t7 = (t5 == (unsigned char)3);
    t1 = t7;
    goto LAB12;

LAB13:    xsi_set_current_line(78, ng0);
    t2 = (t0 + 6132);
    t9 = (t0 + 2248U);
    t11 = *((char **)t9);
    t9 = (t11 + 0);
    memcpy(t9, t2, 4U);
    xsi_set_current_line(79, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB16:    xsi_set_current_line(81, ng0);
    t2 = (t0 + 6136);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(82, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB18:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 10081);
    t1 = t5;
    goto LAB20;

LAB21:    xsi_set_current_line(84, ng0);
    t2 = (t0 + 6140);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(85, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB23:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 15122);
    t1 = t5;
    goto LAB25;

LAB26:    xsi_set_current_line(87, ng0);
    t2 = (t0 + 6144);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(88, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB28:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 20163);
    t1 = t5;
    goto LAB30;

LAB31:    xsi_set_current_line(90, ng0);
    t2 = (t0 + 6148);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(91, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB33:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 25204);
    t1 = t5;
    goto LAB35;

LAB36:    xsi_set_current_line(93, ng0);
    t2 = (t0 + 6152);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(94, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB38:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 30245);
    t1 = t5;
    goto LAB40;

LAB41:    xsi_set_current_line(96, ng0);
    t2 = (t0 + 6156);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(97, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB43:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 35286);
    t1 = t5;
    goto LAB45;

LAB46:    xsi_set_current_line(99, ng0);
    t2 = (t0 + 6160);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(100, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB48:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 40327);
    t1 = t5;
    goto LAB50;

LAB51:    xsi_set_current_line(102, ng0);
    t2 = (t0 + 6164);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(103, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB53:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 45368);
    t1 = t5;
    goto LAB55;

LAB56:    xsi_set_current_line(105, ng0);
    t2 = (t0 + 6168);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

LAB58:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 50409);
    t1 = t5;
    goto LAB60;

LAB61:    xsi_set_current_line(108, ng0);
    t2 = (t0 + 6172);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(109, ng0);
    t2 = (t0 + 6176);
    t6 = (t0 + 2368U);
    t9 = *((char **)t6);
    t6 = (t9 + 0);
    memcpy(t6, t2, 4U);
    goto LAB14;

LAB63:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 55450);
    t1 = t5;
    goto LAB65;

LAB66:    xsi_set_current_line(111, ng0);
    t2 = (t0 + 6180);
    t11 = (t0 + 2248U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    memcpy(t11, t2, 4U);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 6184);
    t6 = (t0 + 2368U);
    t9 = *((char **)t6);
    t6 = (t9 + 0);
    memcpy(t6, t2, 4U);
    goto LAB14;

LAB68:    t2 = (t0 + 2128U);
    t6 = *((char **)t2);
    t13 = *((int *)t6);
    t5 = (t13 <= 60491);
    t1 = t5;
    goto LAB70;

LAB71:    xsi_set_current_line(114, ng0);
    t2 = (t0 + 6188);
    t9 = (t0 + 2248U);
    t11 = *((char **)t9);
    t9 = (t11 + 0);
    memcpy(t9, t2, 4U);
    xsi_set_current_line(115, ng0);
    t2 = (t0 + 2248U);
    t3 = *((char **)t2);
    t2 = (t0 + 2368U);
    t6 = *((char **)t2);
    t2 = (t6 + 0);
    memcpy(t2, t3, 4U);
    goto LAB14;

}


extern void work_a_0249809388_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0249809388_3212880686_p_0};
	xsi_register_didat("work_a_0249809388_3212880686", "isim/blackjack_tb_isim_beh.exe.sim/work/a_0249809388_3212880686.didat");
	xsi_register_executes(pe);
}
