/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/.Xilinx/sed-project/blackJack/FSM.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_3643194780_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;

LAB0:    xsi_set_current_line(55, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 1312U);
    t3 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t3 != 0)
        goto LAB5;

LAB6:
LAB3:    t1 = (t0 + 3808);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(56, ng0);
    t1 = (t0 + 3920);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB5:    xsi_set_current_line(58, ng0);
    t2 = (t0 + 1832U);
    t5 = *((char **)t2);
    t4 = *((unsigned char *)t5);
    t2 = (t0 + 3920);
    t6 = (t2 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    *((unsigned char *)t9) = t4;
    xsi_driver_first_trans_fast(t2);
    goto LAB3;

}

static void work_a_3643194780_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    static char *nl0[] = {&&LAB3, &&LAB4, &&LAB5, &&LAB6, &&LAB7, &&LAB8};

LAB0:    xsi_set_current_line(65, ng0);
    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB2:    t1 = (t0 + 3824);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(66, ng0);
    t4 = (t0 + 1032U);
    t5 = *((char **)t4);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t4 = (t5 + t9);
    t10 = *((unsigned char *)t4);
    t11 = (t10 == (unsigned char)3);
    if (t11 != 0)
        goto LAB10;

LAB12:    xsi_set_current_line(70, ng0);
    t1 = (t0 + 3984);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    *((unsigned char *)t12) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);

LAB11:    goto LAB2;

LAB4:    xsi_set_current_line(73, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB13;

LAB15:    xsi_set_current_line(76, ng0);
    t1 = (t0 + 3984);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    *((unsigned char *)t12) = (unsigned char)1;
    xsi_driver_first_trans_fast(t1);

LAB14:    goto LAB2;

LAB5:    xsi_set_current_line(79, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB16;

LAB18:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6104);
    t10 = 1;
    if (4U == 4U)
        goto LAB24;

LAB25:    t10 = 0;

LAB26:    if (t10 == 1)
        goto LAB21;

LAB22:    t13 = (t0 + 1032U);
    t14 = *((char **)t13);
    t13 = (t0 + 6108);
    t11 = 1;
    if (4U == 4U)
        goto LAB30;

LAB31:    t11 = 0;

LAB32:    t3 = t11;

LAB23:    if (t3 != 0)
        goto LAB19;

LAB20:    xsi_set_current_line(85, ng0);
    t1 = (t0 + 3984);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    *((unsigned char *)t12) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);

LAB17:    goto LAB2;

LAB6:    xsi_set_current_line(88, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB36;

LAB38:    xsi_set_current_line(91, ng0);
    t1 = (t0 + 3984);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    *((unsigned char *)t12) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);

LAB37:    goto LAB2;

LAB7:    xsi_set_current_line(94, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB39;

LAB41:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6112);
    t10 = 1;
    if (4U == 4U)
        goto LAB47;

LAB48:    t10 = 0;

LAB49:    if (t10 == 1)
        goto LAB44;

LAB45:    t13 = (t0 + 1032U);
    t14 = *((char **)t13);
    t13 = (t0 + 6116);
    t11 = 1;
    if (4U == 4U)
        goto LAB53;

LAB54:    t11 = 0;

LAB55:    t3 = t11;

LAB46:    if (t3 != 0)
        goto LAB42;

LAB43:    xsi_set_current_line(99, ng0);
    t1 = (t0 + 3984);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    *((unsigned char *)t12) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);

LAB40:    goto LAB2;

LAB8:    xsi_set_current_line(102, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB59;

LAB61:    xsi_set_current_line(105, ng0);
    t1 = (t0 + 3984);
    t2 = (t1 + 56U);
    t4 = *((char **)t2);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    *((unsigned char *)t12) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);

LAB60:    goto LAB2;

LAB9:    xsi_set_current_line(108, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t6 = (3 - 3);
    t7 = (t6 * -1);
    t8 = (1U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t3 = *((unsigned char *)t1);
    t10 = (t3 == (unsigned char)3);
    if (t10 != 0)
        goto LAB62;

LAB64:
LAB63:    goto LAB2;

LAB10:    xsi_set_current_line(67, ng0);
    t12 = (t0 + 3984);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    *((unsigned char *)t16) = (unsigned char)1;
    xsi_driver_first_trans_fast(t12);
    goto LAB11;

LAB13:    xsi_set_current_line(74, ng0);
    t4 = (t0 + 3984);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)2;
    xsi_driver_first_trans_fast(t4);
    goto LAB14;

LAB16:    xsi_set_current_line(80, ng0);
    t4 = (t0 + 3984);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)2;
    xsi_driver_first_trans_fast(t4);
    goto LAB17;

LAB19:    xsi_set_current_line(83, ng0);
    t18 = (t0 + 3984);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = (unsigned char)3;
    xsi_driver_first_trans_fast(t18);
    goto LAB17;

LAB21:    t3 = (unsigned char)1;
    goto LAB23;

LAB24:    t7 = 0;

LAB27:    if (t7 < 4U)
        goto LAB28;
    else
        goto LAB26;

LAB28:    t5 = (t2 + t7);
    t12 = (t1 + t7);
    if (*((unsigned char *)t5) != *((unsigned char *)t12))
        goto LAB25;

LAB29:    t7 = (t7 + 1);
    goto LAB27;

LAB30:    t8 = 0;

LAB33:    if (t8 < 4U)
        goto LAB34;
    else
        goto LAB32;

LAB34:    t16 = (t14 + t8);
    t17 = (t13 + t8);
    if (*((unsigned char *)t16) != *((unsigned char *)t17))
        goto LAB31;

LAB35:    t8 = (t8 + 1);
    goto LAB33;

LAB36:    xsi_set_current_line(89, ng0);
    t4 = (t0 + 3984);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)4;
    xsi_driver_first_trans_fast(t4);
    goto LAB37;

LAB39:    xsi_set_current_line(95, ng0);
    t4 = (t0 + 3984);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)4;
    xsi_driver_first_trans_fast(t4);
    goto LAB40;

LAB42:    xsi_set_current_line(97, ng0);
    t18 = (t0 + 3984);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = (unsigned char)5;
    xsi_driver_first_trans_fast(t18);
    goto LAB40;

LAB44:    t3 = (unsigned char)1;
    goto LAB46;

LAB47:    t7 = 0;

LAB50:    if (t7 < 4U)
        goto LAB51;
    else
        goto LAB49;

LAB51:    t5 = (t2 + t7);
    t12 = (t1 + t7);
    if (*((unsigned char *)t5) != *((unsigned char *)t12))
        goto LAB48;

LAB52:    t7 = (t7 + 1);
    goto LAB50;

LAB53:    t8 = 0;

LAB56:    if (t8 < 4U)
        goto LAB57;
    else
        goto LAB55;

LAB57:    t16 = (t14 + t8);
    t17 = (t13 + t8);
    if (*((unsigned char *)t16) != *((unsigned char *)t17))
        goto LAB54;

LAB58:    t8 = (t8 + 1);
    goto LAB56;

LAB59:    xsi_set_current_line(103, ng0);
    t1 = (t0 + 3984);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t12 = (t5 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    goto LAB60;

LAB62:    xsi_set_current_line(109, ng0);
    t4 = (t0 + 3984);
    t5 = (t4 + 56U);
    t12 = *((char **)t5);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)0;
    xsi_driver_first_trans_fast(t4);
    goto LAB63;

}

static void work_a_3643194780_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    static char *nl0[] = {&&LAB3, &&LAB4, &&LAB5, &&LAB6, &&LAB7, &&LAB8};

LAB0:    xsi_set_current_line(116, ng0);
    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB2:    t1 = (t0 + 3840);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(117, ng0);
    t4 = (t0 + 6120);
    t6 = (t0 + 4048);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t4, 3U);
    xsi_driver_first_trans_fast_port(t6);
    goto LAB2;

LAB4:    xsi_set_current_line(118, ng0);
    t1 = (t0 + 6123);
    t4 = (t0 + 4048);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB5:    xsi_set_current_line(119, ng0);
    t1 = (t0 + 6126);
    t4 = (t0 + 4048);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB6:    xsi_set_current_line(120, ng0);
    t1 = (t0 + 6129);
    t4 = (t0 + 4048);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB7:    xsi_set_current_line(121, ng0);
    t1 = (t0 + 6132);
    t4 = (t0 + 4048);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB8:    xsi_set_current_line(122, ng0);
    t1 = (t0 + 6135);
    t4 = (t0 + 4048);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB9:    xsi_set_current_line(123, ng0);
    t1 = (t0 + 6138);
    t4 = (t0 + 4048);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

}


extern void work_a_3643194780_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3643194780_3212880686_p_0,(void *)work_a_3643194780_3212880686_p_1,(void *)work_a_3643194780_3212880686_p_2};
	xsi_register_didat("work_a_3643194780_3212880686", "isim/top_tb_isim_beh.exe.sim/work/a_3643194780_3212880686.didat");
	xsi_register_executes(pe);
}
