/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
extern char *IEEE_P_2592010699;
extern char *STD_STANDARD;
extern char *IEEE_P_3620187407;
static const char *ng3 = "/home/daaguirre/Documents/Uni/SED/sed-project/blackJack/blackJack_game.vhd";

unsigned char ieee_p_2592010699_sub_374109322130769762_503743352(char *, unsigned char );
unsigned char ieee_p_3620187407_sub_1366267000076357978_3965413181(char *, char *, char *, char *, char *);
char *ieee_p_3620187407_sub_1496620905533649268_3965413181(char *, char *, char *, char *, char *, char *);


char *work_a_0519386517_3212880686_sub_18112393162295424443_3057020925(char *t1, char *t2, char *t3)
{
    char t4[368];
    char t5[24];
    char t6[16];
    char t11[16];
    char t24[16];
    char t30[8];
    char t37[8];
    char t66[16];
    char t67[16];
    char t69[16];
    char t70[16];
    char t71[16];
    char *t0;
    char *t7;
    char *t8;
    int t9;
    unsigned int t10;
    char *t12;
    char *t13;
    int t14;
    unsigned int t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t25;
    char *t26;
    int t27;
    char *t28;
    char *t29;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t38;
    char *t39;
    char *t40;
    unsigned char t41;
    char *t42;
    int t43;
    int t44;
    char *t45;
    char *t46;
    int t47;
    unsigned int t48;
    unsigned int t49;
    char *t50;
    int t51;
    unsigned int t52;
    unsigned int t53;
    char *t54;
    char *t55;
    char *t56;
    int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    char *t61;
    int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned char t65;
    unsigned char t68;

LAB0:    t7 = (t6 + 0U);
    t8 = (t7 + 0U);
    *((int *)t8) = 7;
    t8 = (t7 + 4U);
    *((int *)t8) = 0;
    t8 = (t7 + 8U);
    *((int *)t8) = -1;
    t9 = (0 - 7);
    t10 = (t9 * -1);
    t10 = (t10 + 1);
    t8 = (t7 + 12U);
    *((unsigned int *)t8) = t10;
    t8 = (t6 + 12U);
    t10 = *((unsigned int *)t8);
    t10 = (t10 * 1U);
    t12 = (t11 + 0U);
    t13 = (t12 + 0U);
    *((int *)t13) = 7;
    t13 = (t12 + 4U);
    *((int *)t13) = 0;
    t13 = (t12 + 8U);
    *((int *)t13) = -1;
    t14 = (0 - 7);
    t15 = (t14 * -1);
    t15 = (t15 + 1);
    t13 = (t12 + 12U);
    *((unsigned int *)t13) = t15;
    t13 = (t4 + 4U);
    t16 = ((IEEE_P_2592010699) + 4000);
    t17 = (t13 + 88U);
    *((char **)t17) = t16;
    t18 = (char *)alloca(t10);
    t19 = (t13 + 56U);
    *((char **)t19) = t18;
    memcpy(t18, t3, t10);
    t20 = (t13 + 64U);
    *((char **)t20) = t11;
    t21 = (t13 + 80U);
    *((unsigned int *)t21) = t10;
    t22 = xsi_get_transient_memory(8U);
    memset(t22, 0, 8U);
    t23 = t22;
    memset(t23, (unsigned char)2, 8U);
    t25 = (t24 + 0U);
    t26 = (t25 + 0U);
    *((int *)t26) = 7;
    t26 = (t25 + 4U);
    *((int *)t26) = 0;
    t26 = (t25 + 8U);
    *((int *)t26) = -1;
    t27 = (0 - 7);
    t15 = (t27 * -1);
    t15 = (t15 + 1);
    t26 = (t25 + 12U);
    *((unsigned int *)t26) = t15;
    t26 = (t4 + 124U);
    t28 = ((IEEE_P_2592010699) + 4000);
    t29 = (t26 + 88U);
    *((char **)t29) = t28;
    t31 = (t26 + 56U);
    *((char **)t31) = t30;
    memcpy(t30, t22, 8U);
    t32 = (t26 + 64U);
    *((char **)t32) = t24;
    t33 = (t26 + 80U);
    *((unsigned int *)t33) = 8U;
    t34 = (t4 + 244U);
    t35 = ((STD_STANDARD) + 384);
    t36 = (t34 + 88U);
    *((char **)t36) = t35;
    t38 = (t34 + 56U);
    *((char **)t38) = t37;
    *((int *)t37) = 0;
    t39 = (t34 + 80U);
    *((unsigned int *)t39) = 4U;
    t40 = (t5 + 4U);
    t41 = (t3 != 0);
    if (t41 == 1)
        goto LAB3;

LAB2:    t42 = (t5 + 12U);
    *((char **)t42) = t6;
    t43 = 0;
    t44 = 7;

LAB4:    if (t43 <= t44)
        goto LAB5;

LAB7:    t7 = (t26 + 56U);
    t8 = *((char **)t7);
    t7 = (t24 + 12U);
    t10 = *((unsigned int *)t7);
    t10 = (t10 * 1U);
    t0 = xsi_get_transient_memory(t10);
    memcpy(t0, t8, t10);
    t12 = (t24 + 0U);
    t9 = *((int *)t12);
    t16 = (t24 + 4U);
    t14 = *((int *)t16);
    t17 = (t24 + 8U);
    t27 = *((int *)t17);
    t19 = (t2 + 0U);
    t20 = (t19 + 0U);
    *((int *)t20) = t9;
    t20 = (t19 + 4U);
    *((int *)t20) = t14;
    t20 = (t19 + 8U);
    *((int *)t20) = t27;
    t43 = (t14 - t9);
    t15 = (t43 * t27);
    t15 = (t15 + 1);
    t20 = (t19 + 12U);
    *((unsigned int *)t20) = t15;

LAB1:    return t0;
LAB3:    *((char **)t40) = t3;
    goto LAB2;

LAB5:    t45 = (t26 + 56U);
    t46 = *((char **)t45);
    t45 = (t24 + 0U);
    t47 = *((int *)t45);
    t15 = (t47 - 6);
    t48 = (t15 * 1U);
    t49 = (0 + t48);
    t50 = (t46 + t49);
    t51 = (0 - 6);
    t52 = (t51 * -1);
    t52 = (t52 + 1);
    t53 = (1U * t52);
    t54 = xsi_get_transient_memory(t53);
    memcpy(t54, t50, t53);
    t55 = (t26 + 56U);
    t56 = *((char **)t55);
    t55 = (t24 + 0U);
    t57 = *((int *)t55);
    t58 = (t57 - 7);
    t59 = (t58 * 1U);
    t60 = (0 + t59);
    t61 = (t56 + t60);
    t62 = (0 - 6);
    t63 = (t62 * -1);
    t63 = (t63 + 1);
    t64 = (1U * t63);
    memcpy(t61, t54, t64);
    t7 = (t13 + 56U);
    t8 = *((char **)t7);
    t7 = (t11 + 0U);
    t9 = *((int *)t7);
    t12 = (t11 + 8U);
    t14 = *((int *)t12);
    t27 = (7 - t9);
    t10 = (t27 * t14);
    t15 = (1U * t10);
    t48 = (0 + t15);
    t16 = (t8 + t48);
    t41 = *((unsigned char *)t16);
    t17 = (t26 + 56U);
    t19 = *((char **)t17);
    t17 = (t24 + 0U);
    t47 = *((int *)t17);
    t20 = (t24 + 8U);
    t51 = *((int *)t20);
    t57 = (0 - t47);
    t49 = (t57 * t51);
    t52 = (1U * t49);
    t53 = (0 + t52);
    t21 = (t19 + t53);
    *((unsigned char *)t21) = t41;
    t7 = (t13 + 56U);
    t8 = *((char **)t7);
    t7 = (t11 + 0U);
    t9 = *((int *)t7);
    t10 = (t9 - 6);
    t15 = (t10 * 1U);
    t48 = (0 + t15);
    t12 = (t8 + t48);
    t14 = (0 - 6);
    t49 = (t14 * -1);
    t49 = (t49 + 1);
    t52 = (1U * t49);
    t16 = xsi_get_transient_memory(t52);
    memcpy(t16, t12, t52);
    t17 = (t13 + 56U);
    t19 = *((char **)t17);
    t17 = (t11 + 0U);
    t27 = *((int *)t17);
    t53 = (t27 - 7);
    t58 = (t53 * 1U);
    t59 = (0 + t58);
    t20 = (t19 + t59);
    t47 = (0 - 6);
    t60 = (t47 * -1);
    t60 = (t60 + 1);
    t63 = (1U * t60);
    memcpy(t20, t16, t63);
    t7 = (t13 + 56U);
    t8 = *((char **)t7);
    t7 = (t11 + 0U);
    t9 = *((int *)t7);
    t12 = (t11 + 8U);
    t14 = *((int *)t12);
    t27 = (0 - t9);
    t10 = (t27 * t14);
    t15 = (1U * t10);
    t48 = (0 + t15);
    t16 = (t8 + t48);
    *((unsigned char *)t16) = (unsigned char)2;
    t65 = (t43 < 7);
    if (t65 == 1)
        goto LAB11;

LAB12:    t41 = (unsigned char)0;

LAB13:    if (t41 != 0)
        goto LAB8;

LAB10:
LAB9:    t65 = (t43 < 7);
    if (t65 == 1)
        goto LAB17;

LAB18:    t41 = (unsigned char)0;

LAB19:    if (t41 != 0)
        goto LAB14;

LAB16:
LAB15:
LAB6:    if (t43 == t44)
        goto LAB7;

LAB20:    t9 = (t43 + 1);
    t43 = t9;
    goto LAB4;

LAB8:    t21 = (t26 + 56U);
    t22 = *((char **)t21);
    t21 = (t24 + 0U);
    t47 = *((int *)t21);
    t49 = (t47 - 3);
    t52 = (t49 * 1U);
    t53 = (0 + t52);
    t23 = (t22 + t53);
    t25 = (t70 + 0U);
    t28 = (t25 + 0U);
    *((int *)t28) = 3;
    t28 = (t25 + 4U);
    *((int *)t28) = 0;
    t28 = (t25 + 8U);
    *((int *)t28) = -1;
    t51 = (0 - 3);
    t58 = (t51 * -1);
    t58 = (t58 + 1);
    t28 = (t25 + 12U);
    *((unsigned int *)t28) = t58;
    t28 = (t1 + 15785);
    t31 = (t71 + 0U);
    t32 = (t31 + 0U);
    *((int *)t32) = 0;
    t32 = (t31 + 4U);
    *((int *)t32) = 3;
    t32 = (t31 + 8U);
    *((int *)t32) = 1;
    t57 = (3 - 0);
    t58 = (t57 * 1);
    t58 = (t58 + 1);
    t32 = (t31 + 12U);
    *((unsigned int *)t32) = t58;
    t32 = ieee_p_3620187407_sub_1496620905533649268_3965413181(IEEE_P_3620187407, t69, t23, t70, t28, t71);
    t33 = (t26 + 56U);
    t35 = *((char **)t33);
    t33 = (t24 + 0U);
    t62 = *((int *)t33);
    t58 = (t62 - 3);
    t59 = (t58 * 1U);
    t60 = (0 + t59);
    t36 = (t35 + t60);
    t38 = (t69 + 12U);
    t63 = *((unsigned int *)t38);
    t64 = (1U * t63);
    memcpy(t36, t32, t64);
    goto LAB9;

LAB11:    t7 = (t26 + 56U);
    t8 = *((char **)t7);
    t7 = (t24 + 0U);
    t9 = *((int *)t7);
    t10 = (t9 - 3);
    t15 = (t10 * 1U);
    t48 = (0 + t15);
    t12 = (t8 + t48);
    t16 = (t66 + 0U);
    t17 = (t16 + 0U);
    *((int *)t17) = 3;
    t17 = (t16 + 4U);
    *((int *)t17) = 0;
    t17 = (t16 + 8U);
    *((int *)t17) = -1;
    t14 = (0 - 3);
    t49 = (t14 * -1);
    t49 = (t49 + 1);
    t17 = (t16 + 12U);
    *((unsigned int *)t17) = t49;
    t17 = (t1 + 15781);
    t20 = (t67 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = 0;
    t21 = (t20 + 4U);
    *((int *)t21) = 3;
    t21 = (t20 + 8U);
    *((int *)t21) = 1;
    t27 = (3 - 0);
    t49 = (t27 * 1);
    t49 = (t49 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t49;
    t68 = ieee_std_logic_unsigned_greater_stdv_stdv(IEEE_P_3620187407, t12, t66, t17, t67);
    t41 = t68;
    goto LAB13;

LAB14:    t21 = (t26 + 56U);
    t22 = *((char **)t21);
    t21 = (t24 + 0U);
    t47 = *((int *)t21);
    t49 = (t47 - 7);
    t52 = (t49 * 1U);
    t53 = (0 + t52);
    t23 = (t22 + t53);
    t25 = (t70 + 0U);
    t28 = (t25 + 0U);
    *((int *)t28) = 7;
    t28 = (t25 + 4U);
    *((int *)t28) = 4;
    t28 = (t25 + 8U);
    *((int *)t28) = -1;
    t51 = (4 - 7);
    t58 = (t51 * -1);
    t58 = (t58 + 1);
    t28 = (t25 + 12U);
    *((unsigned int *)t28) = t58;
    t28 = (t1 + 15793);
    t31 = (t71 + 0U);
    t32 = (t31 + 0U);
    *((int *)t32) = 0;
    t32 = (t31 + 4U);
    *((int *)t32) = 3;
    t32 = (t31 + 8U);
    *((int *)t32) = 1;
    t57 = (3 - 0);
    t58 = (t57 * 1);
    t58 = (t58 + 1);
    t32 = (t31 + 12U);
    *((unsigned int *)t32) = t58;
    t32 = ieee_p_3620187407_sub_1496620905533649268_3965413181(IEEE_P_3620187407, t69, t23, t70, t28, t71);
    t33 = (t26 + 56U);
    t35 = *((char **)t33);
    t33 = (t24 + 0U);
    t62 = *((int *)t33);
    t58 = (t62 - 7);
    t59 = (t58 * 1U);
    t60 = (0 + t59);
    t36 = (t35 + t60);
    t38 = (t69 + 12U);
    t63 = *((unsigned int *)t38);
    t64 = (1U * t63);
    memcpy(t36, t32, t64);
    goto LAB15;

LAB17:    t7 = (t26 + 56U);
    t8 = *((char **)t7);
    t7 = (t24 + 0U);
    t9 = *((int *)t7);
    t10 = (t9 - 7);
    t15 = (t10 * 1U);
    t48 = (0 + t15);
    t12 = (t8 + t48);
    t16 = (t66 + 0U);
    t17 = (t16 + 0U);
    *((int *)t17) = 7;
    t17 = (t16 + 4U);
    *((int *)t17) = 4;
    t17 = (t16 + 8U);
    *((int *)t17) = -1;
    t14 = (4 - 7);
    t49 = (t14 * -1);
    t49 = (t49 + 1);
    t17 = (t16 + 12U);
    *((unsigned int *)t17) = t49;
    t17 = (t1 + 15789);
    t20 = (t67 + 0U);
    t21 = (t20 + 0U);
    *((int *)t21) = 0;
    t21 = (t20 + 4U);
    *((int *)t21) = 3;
    t21 = (t20 + 8U);
    *((int *)t21) = 1;
    t27 = (3 - 0);
    t49 = (t27 * 1);
    t49 = (t49 + 1);
    t21 = (t20 + 12U);
    *((unsigned int *)t21) = t49;
    t68 = ieee_std_logic_unsigned_greater_stdv_stdv(IEEE_P_3620187407, t12, t66, t17, t67);
    t41 = t68;
    goto LAB19;

LAB21:;
}

char *work_a_0519386517_3212880686_sub_543318248355667419_3057020925(char *t1, char *t2, char *t3)
{
    char t4[128];
    char t5[24];
    char t6[16];
    char t11[16];
    char t16[8];
    char *t0;
    char *t7;
    char *t8;
    int t9;
    unsigned int t10;
    char *t12;
    int t13;
    char *t14;
    char *t15;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    unsigned char t21;
    char *t22;
    char *t23;
    int t25;
    char *t26;
    int t28;
    char *t29;
    int t31;
    char *t32;
    int t34;
    char *t35;
    int t37;
    char *t38;
    int t40;
    char *t41;
    int t43;
    char *t44;
    int t46;
    char *t47;
    int t49;
    char *t50;
    int t52;
    char *t53;
    char *t55;
    char *t56;
    unsigned int t57;

LAB0:    t7 = (t6 + 0U);
    t8 = (t7 + 0U);
    *((int *)t8) = 3;
    t8 = (t7 + 4U);
    *((int *)t8) = 0;
    t8 = (t7 + 8U);
    *((int *)t8) = -1;
    t9 = (0 - 3);
    t10 = (t9 * -1);
    t10 = (t10 + 1);
    t8 = (t7 + 12U);
    *((unsigned int *)t8) = t10;
    t8 = (t11 + 0U);
    t12 = (t8 + 0U);
    *((int *)t12) = 6;
    t12 = (t8 + 4U);
    *((int *)t12) = 0;
    t12 = (t8 + 8U);
    *((int *)t12) = -1;
    t13 = (0 - 6);
    t10 = (t13 * -1);
    t10 = (t10 + 1);
    t12 = (t8 + 12U);
    *((unsigned int *)t12) = t10;
    t12 = (t4 + 4U);
    t14 = ((IEEE_P_2592010699) + 4000);
    t15 = (t12 + 88U);
    *((char **)t15) = t14;
    t17 = (t12 + 56U);
    *((char **)t17) = t16;
    xsi_type_set_default_value(t14, t16, t11);
    t18 = (t12 + 64U);
    *((char **)t18) = t11;
    t19 = (t12 + 80U);
    *((unsigned int *)t19) = 7U;
    t20 = (t5 + 4U);
    t21 = (t3 != 0);
    if (t21 == 1)
        goto LAB3;

LAB2:    t22 = (t5 + 12U);
    *((char **)t22) = t6;
    t23 = (t1 + 15797);
    t25 = xsi_mem_cmp(t23, t3, 4U);
    if (t25 == 1)
        goto LAB5;

LAB16:    t26 = (t1 + 15801);
    t28 = xsi_mem_cmp(t26, t3, 4U);
    if (t28 == 1)
        goto LAB6;

LAB17:    t29 = (t1 + 15805);
    t31 = xsi_mem_cmp(t29, t3, 4U);
    if (t31 == 1)
        goto LAB7;

LAB18:    t32 = (t1 + 15809);
    t34 = xsi_mem_cmp(t32, t3, 4U);
    if (t34 == 1)
        goto LAB8;

LAB19:    t35 = (t1 + 15813);
    t37 = xsi_mem_cmp(t35, t3, 4U);
    if (t37 == 1)
        goto LAB9;

LAB20:    t38 = (t1 + 15817);
    t40 = xsi_mem_cmp(t38, t3, 4U);
    if (t40 == 1)
        goto LAB10;

LAB21:    t41 = (t1 + 15821);
    t43 = xsi_mem_cmp(t41, t3, 4U);
    if (t43 == 1)
        goto LAB11;

LAB22:    t44 = (t1 + 15825);
    t46 = xsi_mem_cmp(t44, t3, 4U);
    if (t46 == 1)
        goto LAB12;

LAB23:    t47 = (t1 + 15829);
    t49 = xsi_mem_cmp(t47, t3, 4U);
    if (t49 == 1)
        goto LAB13;

LAB24:    t50 = (t1 + 15833);
    t52 = xsi_mem_cmp(t50, t3, 4U);
    if (t52 == 1)
        goto LAB14;

LAB25:
LAB15:    t7 = (t1 + 15907);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);

LAB4:    t7 = (t12 + 56U);
    t8 = *((char **)t7);
    t7 = (t11 + 12U);
    t10 = *((unsigned int *)t7);
    t10 = (t10 * 1U);
    t0 = xsi_get_transient_memory(t10);
    memcpy(t0, t8, t10);
    t14 = (t11 + 0U);
    t9 = *((int *)t14);
    t15 = (t11 + 4U);
    t13 = *((int *)t15);
    t17 = (t11 + 8U);
    t25 = *((int *)t17);
    t18 = (t2 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = t9;
    t19 = (t18 + 4U);
    *((int *)t19) = t13;
    t19 = (t18 + 8U);
    *((int *)t19) = t25;
    t28 = (t13 - t9);
    t57 = (t28 * t25);
    t57 = (t57 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t57;

LAB1:    return t0;
LAB3:    *((char **)t20) = t3;
    goto LAB2;

LAB5:    t53 = (t1 + 15837);
    t55 = (t12 + 56U);
    t56 = *((char **)t55);
    t55 = (t56 + 0);
    memcpy(t55, t53, 7U);
    goto LAB4;

LAB6:    t7 = (t1 + 15844);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB7:    t7 = (t1 + 15851);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB8:    t7 = (t1 + 15858);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB9:    t7 = (t1 + 15865);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB10:    t7 = (t1 + 15872);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB11:    t7 = (t1 + 15879);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB12:    t7 = (t1 + 15886);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB13:    t7 = (t1 + 15893);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB14:    t7 = (t1 + 15900);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t14 = (t15 + 0);
    memcpy(t14, t7, 7U);
    goto LAB4;

LAB26:;
LAB27:;
}

static void work_a_0519386517_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(124, ng3);

LAB3:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 9176);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 3U, 1, 0LL);

LAB2:    t8 = (t0 + 8968);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(125, ng3);

LAB3:    t1 = (t0 + 2632U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 9240);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 2U, 1, 0LL);

LAB2:    t8 = (t0 + 8984);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(126, ng3);

LAB3:    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 9304);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 1U, 1, 0LL);

LAB2:    t8 = (t0 + 9000);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(127, ng3);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 9368);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 0U, 1, 0LL);

LAB2:    t8 = (t0 + 9016);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_4(char *t0)
{
    char t5[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    int t8;
    unsigned int t9;
    unsigned char t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    unsigned char t17;
    unsigned char t18;
    unsigned char t19;
    unsigned char t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    unsigned char t29;
    unsigned char t30;

LAB0:    xsi_set_current_line(155, ng3);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 15352U);
    t3 = (t0 + 15914);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 15352U);
    t3 = (t0 + 15945);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB5;

LAB6:    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 15352U);
    t3 = (t0 + 15976);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB7;

LAB8:    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 15352U);
    t3 = (t0 + 15987);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB26;

LAB27:    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 15352U);
    t3 = (t0 + 16018);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB28;

LAB29:    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 15352U);
    t3 = (t0 + 16029);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 2;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (2 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB47;

LAB48:
LAB3:    t1 = (t0 + 9032);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(157, ng3);
    t7 = (t0 + 15917);
    t12 = (t0 + 9432);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 7U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(158, ng3);
    t1 = (t0 + 15924);
    t3 = (t0 + 9496);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(159, ng3);
    t1 = (t0 + 15931);
    t3 = (t0 + 9560);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(160, ng3);
    t1 = (t0 + 15938);
    t3 = (t0 + 9624);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB3;

LAB5:    xsi_set_current_line(163, ng3);
    t7 = (t0 + 15948);
    t12 = (t0 + 9432);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 7U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(164, ng3);
    t1 = (t0 + 15955);
    t3 = (t0 + 9496);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(165, ng3);
    t1 = (t0 + 15962);
    t3 = (t0 + 9560);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(166, ng3);
    t1 = (t0 + 15969);
    t3 = (t0 + 9624);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB3;

LAB7:    xsi_set_current_line(168, ng3);
    t7 = (t0 + 1032U);
    t11 = *((char **)t7);
    t17 = *((unsigned char *)t11);
    t18 = (t17 == (unsigned char)3);
    if (t18 != 0)
        goto LAB9;

LAB11:
LAB10:    goto LAB3;

LAB9:    xsi_set_current_line(169, ng3);
    t7 = (t0 + 5688U);
    t12 = *((char **)t7);
    t19 = *((unsigned char *)t12);
    t20 = ieee_p_2592010699_sub_374109322130769762_503743352(IEEE_P_2592010699, t19);
    t7 = (t0 + 5688U);
    t13 = *((char **)t7);
    t7 = (t13 + 0);
    *((unsigned char *)t7) = t20;
    xsi_set_current_line(170, ng3);
    t1 = (t0 + 5688U);
    t2 = *((char **)t1);
    t10 = *((unsigned char *)t2);
    t17 = (t10 == (unsigned char)3);
    if (t17 != 0)
        goto LAB12;

LAB14:
LAB13:    goto LAB10;

LAB12:    xsi_set_current_line(171, ng3);
    t1 = (t0 + 4392U);
    t3 = *((char **)t1);
    t1 = (t0 + 5088U);
    t4 = *((char **)t1);
    t9 = (7 - 3);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t4 + t22);
    memcpy(t1, t3, 4U);
    xsi_set_current_line(172, ng3);
    t1 = (t0 + 5088U);
    t2 = *((char **)t1);
    t1 = (t0 + 15560U);
    t3 = (t0 + 4848U);
    t4 = *((char **)t3);
    t3 = (t0 + 15528U);
    t6 = ieee_p_3620187407_sub_1496620905533649268_3965413181(IEEE_P_3620187407, t5, t2, t1, t4, t3);
    t7 = (t0 + 4848U);
    t11 = *((char **)t7);
    t7 = (t11 + 0);
    t12 = (t5 + 12U);
    t9 = *((unsigned int *)t12);
    t21 = (1U * t9);
    memcpy(t7, t6, t21);
    xsi_set_current_line(173, ng3);
    t1 = (t0 + 4848U);
    t2 = *((char **)t1);
    t1 = work_a_0519386517_3212880686_sub_18112393162295424443_3057020925(t0, t5, t2);
    t3 = (t0 + 5208U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    t6 = (t5 + 12U);
    t9 = *((unsigned int *)t6);
    t9 = (t9 * 1U);
    memcpy(t3, t1, t9);
    xsi_set_current_line(174, ng3);
    t1 = (t0 + 5208U);
    t2 = *((char **)t1);
    t9 = (7 - 3);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t2 + t22);
    t3 = work_a_0519386517_3212880686_sub_543318248355667419_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t23 = *((unsigned int *)t4);
    t23 = (t23 * 1U);
    t10 = (7U != t23);
    if (t10 == 1)
        goto LAB15;

LAB16:    t6 = (t0 + 9624);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(175, ng3);
    t1 = (t0 + 5208U);
    t2 = *((char **)t1);
    t9 = (7 - 7);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t2 + t22);
    t3 = work_a_0519386517_3212880686_sub_543318248355667419_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t23 = *((unsigned int *)t4);
    t23 = (t23 * 1U);
    t10 = (7U != t23);
    if (t10 == 1)
        goto LAB17;

LAB18:    t6 = (t0 + 9560);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(176, ng3);
    t1 = xsi_get_transient_memory(8U);
    memset(t1, 0, 8U);
    t2 = t1;
    memset(t2, (unsigned char)2, 8U);
    t3 = (t0 + 5088U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    memcpy(t3, t1, 8U);
    xsi_set_current_line(177, ng3);
    t1 = (t0 + 4232U);
    t2 = *((char **)t1);
    t1 = (t0 + 5088U);
    t3 = *((char **)t1);
    t9 = (7 - 3);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t3 + t22);
    memcpy(t1, t2, 4U);
    xsi_set_current_line(178, ng3);
    t1 = (t0 + 5088U);
    t2 = *((char **)t1);
    t1 = work_a_0519386517_3212880686_sub_18112393162295424443_3057020925(t0, t5, t2);
    t3 = (t0 + 5328U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    t6 = (t5 + 12U);
    t9 = *((unsigned int *)t6);
    t9 = (t9 * 1U);
    memcpy(t3, t1, t9);
    xsi_set_current_line(179, ng3);
    t1 = (t0 + 5328U);
    t2 = *((char **)t1);
    t9 = (7 - 3);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t2 + t22);
    t3 = work_a_0519386517_3212880686_sub_543318248355667419_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t23 = *((unsigned int *)t4);
    t23 = (t23 * 1U);
    t10 = (7U != t23);
    if (t10 == 1)
        goto LAB19;

LAB20:    t6 = (t0 + 9496);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(180, ng3);
    t1 = (t0 + 5328U);
    t2 = *((char **)t1);
    t9 = (7 - 7);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t2 + t22);
    t3 = work_a_0519386517_3212880686_sub_543318248355667419_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t23 = *((unsigned int *)t4);
    t23 = (t23 * 1U);
    t10 = (7U != t23);
    if (t10 == 1)
        goto LAB21;

LAB22:    t6 = (t0 + 9432);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(181, ng3);
    t1 = (t0 + 4848U);
    t2 = *((char **)t1);
    t1 = (t0 + 15528U);
    t3 = (t0 + 15979);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 7;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (7 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_greater_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB23;

LAB25:
LAB24:    goto LAB13;

LAB15:    xsi_size_not_matching(7U, t23, 0);
    goto LAB16;

LAB17:    xsi_size_not_matching(7U, t23, 0);
    goto LAB18;

LAB19:    xsi_size_not_matching(7U, t23, 0);
    goto LAB20;

LAB21:    xsi_size_not_matching(7U, t23, 0);
    goto LAB22;

LAB23:    xsi_set_current_line(182, ng3);
    t7 = (t0 + 9688);
    t11 = (t7 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_fast(t7);
    goto LAB24;

LAB26:    xsi_set_current_line(188, ng3);
    t7 = (t0 + 15990);
    t12 = (t0 + 9432);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t7, 7U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(189, ng3);
    t1 = (t0 + 15997);
    t3 = (t0 + 9496);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(190, ng3);
    t1 = (t0 + 16004);
    t3 = (t0 + 9560);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(191, ng3);
    t1 = (t0 + 16011);
    t3 = (t0 + 9624);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(192, ng3);
    t1 = (t0 + 5688U);
    t2 = *((char **)t1);
    t1 = (t2 + 0);
    *((unsigned char *)t1) = (unsigned char)2;
    goto LAB3;

LAB28:    xsi_set_current_line(195, ng3);
    t7 = (t0 + 1032U);
    t11 = *((char **)t7);
    t17 = *((unsigned char *)t11);
    t18 = (t17 == (unsigned char)3);
    if (t18 != 0)
        goto LAB30;

LAB32:
LAB31:    goto LAB3;

LAB30:    xsi_set_current_line(196, ng3);
    t7 = (t0 + 5688U);
    t12 = *((char **)t7);
    t19 = *((unsigned char *)t12);
    t20 = ieee_p_2592010699_sub_374109322130769762_503743352(IEEE_P_2592010699, t19);
    t7 = (t0 + 5688U);
    t13 = *((char **)t7);
    t7 = (t13 + 0);
    *((unsigned char *)t7) = t20;
    xsi_set_current_line(197, ng3);
    t1 = (t0 + 5688U);
    t2 = *((char **)t1);
    t10 = *((unsigned char *)t2);
    t17 = (t10 == (unsigned char)3);
    if (t17 != 0)
        goto LAB33;

LAB35:
LAB34:    goto LAB31;

LAB33:    xsi_set_current_line(198, ng3);
    t1 = (t0 + 4968U);
    t3 = *((char **)t1);
    t1 = (t0 + 15544U);
    t4 = (t0 + 4392U);
    t6 = *((char **)t4);
    t4 = (t0 + 15512U);
    t7 = ieee_p_3620187407_sub_1496620905533649268_3965413181(IEEE_P_3620187407, t5, t3, t1, t6, t4);
    t11 = (t0 + 4968U);
    t12 = *((char **)t11);
    t11 = (t12 + 0);
    t13 = (t5 + 12U);
    t9 = *((unsigned int *)t13);
    t21 = (1U * t9);
    memcpy(t11, t7, t21);
    xsi_set_current_line(199, ng3);
    t1 = (t0 + 4968U);
    t2 = *((char **)t1);
    t1 = work_a_0519386517_3212880686_sub_18112393162295424443_3057020925(t0, t5, t2);
    t3 = (t0 + 5208U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    t6 = (t5 + 12U);
    t9 = *((unsigned int *)t6);
    t9 = (t9 * 1U);
    memcpy(t3, t1, t9);
    xsi_set_current_line(200, ng3);
    t1 = (t0 + 5208U);
    t2 = *((char **)t1);
    t9 = (7 - 3);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t2 + t22);
    t3 = work_a_0519386517_3212880686_sub_543318248355667419_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t23 = *((unsigned int *)t4);
    t23 = (t23 * 1U);
    t10 = (7U != t23);
    if (t10 == 1)
        goto LAB36;

LAB37:    t6 = (t0 + 9624);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(201, ng3);
    t1 = (t0 + 5208U);
    t2 = *((char **)t1);
    t9 = (7 - 7);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t2 + t22);
    t3 = work_a_0519386517_3212880686_sub_543318248355667419_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t23 = *((unsigned int *)t4);
    t23 = (t23 * 1U);
    t10 = (7U != t23);
    if (t10 == 1)
        goto LAB38;

LAB39:    t6 = (t0 + 9560);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(202, ng3);
    t1 = xsi_get_transient_memory(8U);
    memset(t1, 0, 8U);
    t2 = t1;
    memset(t2, (unsigned char)2, 8U);
    t3 = (t0 + 5088U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    memcpy(t3, t1, 8U);
    xsi_set_current_line(203, ng3);
    t1 = (t0 + 4232U);
    t2 = *((char **)t1);
    t1 = (t0 + 5088U);
    t3 = *((char **)t1);
    t9 = (7 - 3);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t3 + t22);
    memcpy(t1, t2, 4U);
    xsi_set_current_line(204, ng3);
    t1 = (t0 + 5088U);
    t2 = *((char **)t1);
    t1 = work_a_0519386517_3212880686_sub_18112393162295424443_3057020925(t0, t5, t2);
    t3 = (t0 + 5328U);
    t4 = *((char **)t3);
    t3 = (t4 + 0);
    t6 = (t5 + 12U);
    t9 = *((unsigned int *)t6);
    t9 = (t9 * 1U);
    memcpy(t3, t1, t9);
    xsi_set_current_line(205, ng3);
    t1 = (t0 + 5328U);
    t2 = *((char **)t1);
    t9 = (7 - 3);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t2 + t22);
    t3 = work_a_0519386517_3212880686_sub_543318248355667419_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t23 = *((unsigned int *)t4);
    t23 = (t23 * 1U);
    t10 = (7U != t23);
    if (t10 == 1)
        goto LAB40;

LAB41:    t6 = (t0 + 9496);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(206, ng3);
    t1 = (t0 + 5328U);
    t2 = *((char **)t1);
    t9 = (7 - 7);
    t21 = (t9 * 1U);
    t22 = (0 + t21);
    t1 = (t2 + t22);
    t3 = work_a_0519386517_3212880686_sub_543318248355667419_3057020925(t0, t5, t1);
    t4 = (t5 + 12U);
    t23 = *((unsigned int *)t4);
    t23 = (t23 * 1U);
    t10 = (7U != t23);
    if (t10 == 1)
        goto LAB42;

LAB43:    t6 = (t0 + 9432);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t3, 7U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(207, ng3);
    t1 = (t0 + 4968U);
    t2 = *((char **)t1);
    t1 = (t0 + 15544U);
    t3 = (t0 + 16021);
    t6 = (t5 + 0U);
    t7 = (t6 + 0U);
    *((int *)t7) = 0;
    t7 = (t6 + 4U);
    *((int *)t7) = 7;
    t7 = (t6 + 8U);
    *((int *)t7) = 1;
    t8 = (7 - 0);
    t9 = (t8 * 1);
    t9 = (t9 + 1);
    t7 = (t6 + 12U);
    *((unsigned int *)t7) = t9;
    t10 = ieee_std_logic_unsigned_greater_stdv_stdv(IEEE_P_3620187407, t2, t1, t3, t5);
    if (t10 != 0)
        goto LAB44;

LAB46:
LAB45:    goto LAB34;

LAB36:    xsi_size_not_matching(7U, t23, 0);
    goto LAB37;

LAB38:    xsi_size_not_matching(7U, t23, 0);
    goto LAB39;

LAB40:    xsi_size_not_matching(7U, t23, 0);
    goto LAB41;

LAB42:    xsi_size_not_matching(7U, t23, 0);
    goto LAB43;

LAB44:    xsi_set_current_line(208, ng3);
    t7 = (t0 + 9752);
    t11 = (t7 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = (unsigned char)3;
    xsi_driver_first_trans_fast(t7);
    goto LAB45;

LAB47:    xsi_set_current_line(213, ng3);
    t7 = (t0 + 4848U);
    t11 = *((char **)t7);
    t7 = (t0 + 15528U);
    t12 = (t0 + 4968U);
    t13 = *((char **)t12);
    t12 = (t0 + 15544U);
    t18 = ieee_std_logic_unsigned_greater_stdv_stdv(IEEE_P_3620187407, t11, t7, t13, t12);
    if (t18 == 1)
        goto LAB52;

LAB53:    t17 = (unsigned char)0;

LAB54:    if (t17 != 0)
        goto LAB49;

LAB51:    t1 = (t0 + 4848U);
    t2 = *((char **)t1);
    t1 = (t0 + 15528U);
    t3 = (t0 + 4968U);
    t4 = *((char **)t3);
    t3 = (t0 + 15544U);
    t17 = ieee_p_3620187407_sub_1366267000076357978_3965413181(IEEE_P_3620187407, t2, t1, t4, t3);
    if (t17 == 1)
        goto LAB57;

LAB58:    t10 = (unsigned char)0;

LAB59:    if (t10 != 0)
        goto LAB55;

LAB56:    t1 = (t0 + 4848U);
    t2 = *((char **)t1);
    t1 = (t0 + 15528U);
    t3 = (t0 + 4968U);
    t4 = *((char **)t3);
    t3 = (t0 + 15544U);
    t17 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t2, t1, t4, t3);
    if (t17 == 1)
        goto LAB62;

LAB63:    t6 = (t0 + 2632U);
    t7 = *((char **)t6);
    t19 = *((unsigned char *)t7);
    t20 = (t19 == (unsigned char)3);
    if (t20 == 1)
        goto LAB65;

LAB66:    t18 = (unsigned char)0;

LAB67:    t10 = t18;

LAB64:    if (t10 != 0)
        goto LAB60;

LAB61:
LAB50:    goto LAB3;

LAB49:    xsi_set_current_line(214, ng3);
    t14 = (t0 + 16032);
    t24 = (t0 + 9432);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    t27 = (t26 + 56U);
    t28 = *((char **)t27);
    memcpy(t28, t14, 7U);
    xsi_driver_first_trans_fast(t24);
    xsi_set_current_line(215, ng3);
    t1 = (t0 + 16039);
    t3 = (t0 + 9496);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(216, ng3);
    t1 = (t0 + 16046);
    t3 = (t0 + 9560);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(217, ng3);
    t1 = (t0 + 16053);
    t3 = (t0 + 9624);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB50;

LAB52:    t14 = (t0 + 2632U);
    t15 = *((char **)t14);
    t19 = *((unsigned char *)t15);
    t20 = (t19 == (unsigned char)2);
    t17 = t20;
    goto LAB54;

LAB55:    xsi_set_current_line(220, ng3);
    t6 = (t0 + 16060);
    t12 = (t0 + 9432);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t14 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t6, 7U);
    xsi_driver_first_trans_fast(t12);
    xsi_set_current_line(221, ng3);
    t1 = (t0 + 16067);
    t3 = (t0 + 9496);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(222, ng3);
    t1 = (t0 + 16074);
    t3 = (t0 + 9560);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(223, ng3);
    t1 = (t0 + 16081);
    t3 = (t0 + 9624);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB50;

LAB57:    t6 = (t0 + 2792U);
    t7 = *((char **)t6);
    t18 = *((unsigned char *)t7);
    t19 = (t18 == (unsigned char)2);
    t10 = t19;
    goto LAB59;

LAB60:    xsi_set_current_line(226, ng3);
    t6 = (t0 + 16088);
    t13 = (t0 + 9432);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t24 = *((char **)t16);
    memcpy(t24, t6, 7U);
    xsi_driver_first_trans_fast(t13);
    xsi_set_current_line(227, ng3);
    t1 = (t0 + 16095);
    t3 = (t0 + 9496);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(228, ng3);
    t1 = (t0 + 16102);
    t3 = (t0 + 9560);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    xsi_set_current_line(229, ng3);
    t1 = (t0 + 16109);
    t3 = (t0 + 9624);
    t4 = (t3 + 56U);
    t6 = *((char **)t4);
    t7 = (t6 + 56U);
    t11 = *((char **)t7);
    memcpy(t11, t1, 7U);
    xsi_driver_first_trans_fast(t3);
    goto LAB50;

LAB62:    t10 = (unsigned char)1;
    goto LAB64;

LAB65:    t6 = (t0 + 2792U);
    t11 = *((char **)t6);
    t29 = *((unsigned char *)t11);
    t30 = (t29 == (unsigned char)3);
    t18 = t30;
    goto LAB67;

}

static void work_a_0519386517_3212880686_p_5(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(233, ng3);

LAB3:    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t1 = (t0 + 9816);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 7U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9048);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_6(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(234, ng3);

LAB3:    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t1 = (t0 + 9880);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 7U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9064);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_7(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(235, ng3);

LAB3:    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t1 = (t0 + 9944);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 7U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9080);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_0519386517_3212880686_p_8(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(236, ng3);

LAB3:    t1 = (t0 + 3432U);
    t2 = *((char **)t1);
    t1 = (t0 + 10008);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 7U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 9096);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_0519386517_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0519386517_3212880686_p_0,(void *)work_a_0519386517_3212880686_p_1,(void *)work_a_0519386517_3212880686_p_2,(void *)work_a_0519386517_3212880686_p_3,(void *)work_a_0519386517_3212880686_p_4,(void *)work_a_0519386517_3212880686_p_5,(void *)work_a_0519386517_3212880686_p_6,(void *)work_a_0519386517_3212880686_p_7,(void *)work_a_0519386517_3212880686_p_8};
	static char *se[] = {(void *)work_a_0519386517_3212880686_sub_18112393162295424443_3057020925,(void *)work_a_0519386517_3212880686_sub_543318248355667419_3057020925};
	xsi_register_didat("work_a_0519386517_3212880686", "isim/top_test_isim_beh.exe.sim/work/a_0519386517_3212880686.didat");
	xsi_register_executes(pe);
	xsi_register_subprogram_executes(se);
}
