----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:45:15 01/12/2016 
-- Design Name: 
-- Module Name:    johnson_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity johnson_counter is
    Port ( 	R    : IN  std_logic; --Reset
				clk  : IN  std_logic; --Senal de reloj        
				data : OUT std_logic_vector(3 downto 0) --Senal con registro de desplazamiento
);
end johnson_counter;

architecture Behavioral of johnson_counter is
	signal temp     : std_logic_vector(3 downto 0) :="1110";
	signal clk_temp : std_logic;
	
	COMPONENT divisor
	PORT(
		R       : IN std_logic; --Reset
		clk_in  : IN std_logic; --Senal de reloj con la frecuancia de trabajo global (5MHz)		
		clk_out : OUT std_logic --Senal de reloj con la una frecuencia de 2KHz
		);
	END COMPONENT;

begin
	data <=temp;
	
	Inst_divisor: divisor PORT MAP(
		R       => R,
		clk_in  => clk,
		clk_out => clk_temp
	);
	
	process(clk_temp,R)
	begin
		if rising_edge(clk_temp) then
			if R = '1' then
				temp <= "1110";
			else 
				temp(3 downto 1) <= temp(2 downto 0);
				temp(0) <= temp(3);
			end if;
		end if;
	end process;
end Behavioral;



