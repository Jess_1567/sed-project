----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:59:00 01/01/2016 
-- Design Name: 
-- Module Name:    blackJack - Behavioral 
-- Project Name: BlackJack simplificado para FPGA
-- Target Devices: 
-- Tool versions: 
-- Description: En este módulo utilizamos la implementación del lfsr para
-- generar las cartas. Este modulo generará aleatoriamente una de las 13 cartas que
-- tiene un palo de la baraja francesa.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity blackJack is
    Port ( 	play  : IN  std_logic; --Boton de accion
				R     : IN  std_logic; --Reset
				clk   : IN  std_logic; --Senal de reloj          
				carta : out STD_LOGIC_VECTOR (3 downto 0); --Registro que lleva la informacion de la carta generada
				valor : out STD_LOGIC_VECTOR (3 downto 0)  --Registro que nos indica el valor de la carta generada para el juego
			  );

end blackJack;

architecture Behavioral of blackJack is
COMPONENT lfsr
		PORT(
			clk  : IN std_logic; --Senal de reloj
			R    : IN std_logic; --Reset       
			data : OUT std_logic_vector(15 downto 0) --Valor aleatorio generado del algoritmo LFSR
			);
		END COMPONENT;

	signal temp : std_logic_vector(15 downto 0);
begin
	Inst_LFSR: lfsr PORT MAP(
		clk  => clk,
		R    => R,
		data => temp
	);
	
	process(play)
	variable value     : integer range 0 to 65536:=65536;
	variable carta_var : std_logic_vector(3 downto 0);
	variable valor_var : std_logic_vector(3 downto 0);
	begin
		if play='1' and R = '0' then
			value := to_integer(unsigned(temp));
		elsif rising_edge(clk) and R = '1' then
			carta_var := "0000";
			valor_var := carta_var;
		end if;
		
		if value <= 5040 then --Rangos que nos permiten obtener la carta aleatoria del AS(1) al REY(13)
				carta_var := "0001";
				valor_var := carta_var;
		elsif value > 5040 and value<=10081 then 
				carta_var := "0010";
				valor_var := carta_var;
		elsif value > 10081 and value<=15122 then
				carta_var := "0011";
				valor_var := carta_var;
		elsif value > 15122 and value<=20163 then
				carta_var := "0100";
				valor_var := carta_var;
		elsif value > 20163 and value<=25204 then
				carta_var := "0101";
				valor_var := carta_var;
		elsif value > 25204 and value<=30245 then
				carta_var := "0110";
				valor_var := carta_var;
		elsif value > 30245 and value<=35286 then
				carta_var := "0111";
				valor_var := carta_var;
		elsif value > 35286 and value<=40327 then
				carta_var := "1000";
				valor_var := carta_var;
		elsif value > 40327 and value<=45368 then
				carta_var := "1001";
				valor_var := carta_var;
		elsif value > 45368 and value<=50409 then
				carta_var := "1010";
				valor_var := carta_var;
		elsif value > 50409 and value<=55450 then
				carta_var := "1011";
				valor_var := "1010";
		elsif value > 55450 and value<=60491 then
				carta_var := "1100";
				valor_var := "1010";
		elsif value = 65536 then 
				carta_var := "0000";
				valor_var := carta_var;
		else
				carta_var := "1101";
				valor_var := "1010";
		end if;
		
		carta <= carta_var;
		valor <= valor_var;
	end process;
	
end Behavioral;

