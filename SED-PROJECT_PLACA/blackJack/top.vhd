----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:59:28 01/12/2016 
-- Design Name: 
-- Module Name:    top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( play      : in   STD_LOGIC; --Boton de accion
           R         : in   STD_LOGIC; --Reset
			  plantarse : in   STD_LOGIC; --Boton de plantarse
           clk       : in   STD_LOGIC; --Senal de reloj
           display   : out  STD_LOGIC_VECTOR (6 downto 0); --Salida al display de 7 segmentos           
			  digctrl   : out  STD_LOGIC_VECTOR (3 downto 0)  --Salida que controla que display se muestra en cada instante
	 ); 
end top;

architecture Behavioral of top is
	signal ctrl_temp        : std_logic_vector(3 downto 0);
	signal display_temp     : std_logic_vector(6 downto 0);
	signal temp1            : std_logic_vector(6 downto 0);
	signal temp2            : std_logic_vector(6 downto 0);
	signal temp3            : std_logic_vector(6 downto 0);
	signal temp4            : std_logic_vector(6 downto 0);
	signal play_flanco 		: std_logic; 
	signal plantarse_flanco	: std_logic; 
	
	COMPONENT blackJack_game
	PORT(
		play      : IN  std_logic; --Boton de accion
      R         : IN  std_logic; --Reset
      plantarse : IN  std_logic; --Boton de plantarse
		clk       : IN  std_logic; --Senal de reloj        
		display1  : OUT std_logic_vector(6 downto 0); --Senales que controlan que 
		display2  : OUT std_logic_vector(6 downto 0); --poner en cada display en 
		display3  : OUT std_logic_vector(6 downto 0); --cada momento
		display4  : OUT std_logic_vector(6 downto 0)  --
		);
	END COMPONENT;
	
	COMPONENT johnson_counter
	PORT(
		R    : IN  std_logic; --Reset
		clk  : IN  std_logic; --Senal de reloj        
		data : OUT std_logic_vector(3 downto 0) --Senal con registro de desplazamiento
		);
	END COMPONENT;
	
	COMPONENT Sinc_y_flanco
	PORT (
		pulsador        : in  STD_LOGIC; --Pulsador 
		clk	          : in  STD_LOGIC; --Senal de reloj
		reset           : in  STD_LOGIC; --Reset
      flanco_positivo : out STD_LOGIC); --Senal acondicionada
	END COMPONENT;
	
begin
	Inst_blackJack_game: blackJack_game PORT MAP(
		play      => play_flanco,
		R         => R,
		plantarse => plantarse_flanco,
		clk       => clk,
		display1  => temp1,
		display2  => temp2,
		display3  => temp3,
		display4  => temp4 
	);
	
	Inst_johnson_counter: johnson_counter PORT MAP(
		R    => R,
		clk  => clk,
		data => ctrl_temp
	);
	
	Inst_sincr: Sinc_y_flanco PORT MAP(
		pulsador        =>  play,
		clk             => clk, 
		reset           => R, 
      flanco_positivo => play_flanco
	); 
	
	Inst_sincr_2: Sinc_y_flanco PORT MAP(
		pulsador        =>  plantarse,
		clk             => clk, 
		reset           => R, 
      flanco_positivo => plantarse_flanco
	); 
	
	process(clk)
	begin
	if rising_edge(clk) then
		if ctrl_temp="0111" then
			display_temp <= temp1;
		elsif ctrl_temp="1011" then
			display_temp <= temp2;
		elsif ctrl_temp="1101" then
			display_temp <= temp3;
		elsif ctrl_temp="1110" then
			display_temp <= temp4;
		else 
			display_temp <= "1111111";
		end if;
	end if;
		
	end process;
	
	display <= display_temp; 
	digctrl <= ctrl_temp;

end Behavioral;

