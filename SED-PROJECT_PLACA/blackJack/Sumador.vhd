----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:06:26 01/21/2016 
-- Design Name: 
-- Module Name:    Sumador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all ;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SumadorJug1 is
    Port ( 	play    : IN  std_logic; --Boton de accion
				estado  : IN  std_logic_vector(2 downto 0); --Senal que lleva la informacion de la maquina de estados global
				clk     : IN  std_logic; --Senal de reloj
				R       : IN  std_logic; --Reset
				sumando : IN  std_logic_vector(3 downto 0); --Valor de la carta que se desea sumar a lo acumulado
				suma    : OUT std_logic_vector(7 downto 0)  --Valor obtenido de la suma entre lo acumulado y el valor pasado por el sumando
	 );
end SumadorJug1; 

architecture Behavioral of SumadorJug1 is
signal suma_parcial: unsigned(7 downto 0); 
signal nueva_suma: unsigned(7 downto 0); 
type states is (x1, x2); 
signal est_actual:states:=x1;
signal est_futuro:states; 

begin

	process(clk,R,estado)
	begin
		if R = '1' then
			est_actual <= x1;
			suma_parcial <= (others => '0');
		elsif rising_edge(clk) then			
			est_actual <= est_futuro; 
			suma_parcial <= nueva_suma;
		end if;
	end process;
	
	
	process (play, est_actual,estado)	
	begin
			est_futuro <= est_actual;
			if est_actual = x1 and play = '1' and estado = "011"  then
			 est_futuro <= x2; 

		elsif est_actual = x2 and play = '0' then
			 est_futuro <= x1; 
		
		end if; 
	end process;
	 
	
	process (est_actual, suma_parcial, sumando)
	begin
		if est_actual = x2 then
			nueva_suma <= suma_parcial + unsigned(sumando); 
		else
			nueva_suma <= suma_parcial; 
		end if;	
	end process; 
	
	suma <= std_logic_vector(suma_parcial); 
end Behavioral;

