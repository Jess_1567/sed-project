----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:53:25 01/02/2016 
-- Design Name: 
-- Module Name:    FSM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM is
    Port ( 	inputs : IN  std_logic_vector(3 downto 0); --Registro que lleva la informacion de las acciones a realizar
				--------------------------------------------------------------------
				--inputs(0) over2     --Gestiona cuando se pasa de 21 el jugador 2--
				--inputs(1) over1     --Gestiona cuando se pasa de 21 el jugador 2--
				--inputs(2) plantarse --Boton de plantarse                        --
				--inputs(3) play      --Boton de accion                           --
				--------------------------------------------------------------------
				R      : IN  std_logic; --Reset
				clk    : IN  std_logic; --Senal de reloj         
				estado : OUT std_logic_vector(2 downto 0) --Salida que nos permite saber en que estado estamos en cada instante
			  );
end FSM;

architecture Behavioral of FSM is
	type estados is (x1,x2,x3,x4,x5,x6);
	signal est_actual : estados := x1;
	signal est_futuro : estados;
	
begin
	
	process(clk,R)
	begin
		if R = '1' then
			est_actual <= x1;
		elsif rising_edge(clk) then
			est_actual <= est_futuro;
		end if;
	end process;
	
	
	process(est_actual,inputs,R)
	begin
			case est_actual is 
				when x1 => if inputs(3) = '1' then
										est_futuro <=	x2;
							
										else 
											est_futuro <= x1;
							  end if;
							  
				when x2 => if inputs(3) = '1'  then
											est_futuro <= x3;
										else
											est_futuro <= x2;
								end if;			
								
				when x3 => if inputs(3) = '1' then
									est_futuro <= x3;
									
							  elsif inputs = "0100" or inputs = "0010" then
									est_futuro <= x4;
								else 
									est_futuro <= x3; 
							  end if;  
							  
				when x4 => if inputs(3) = '1' then
									est_futuro <= x5;
							  else
									est_futuro <= x4;
							  end if;
							  
				when x5 => if inputs(3) = '1' then
									est_futuro <= x5;	
							  elsif inputs = "0100" or inputs = "0001" then
									est_futuro <= x6;
							  else 
									est_futuro <= x5; 
							  end if;
							  
				when x6 => if R = '1' then
									est_futuro <= x1;
							  else 
									est_futuro <= x6;
							  end if;
							  		 
				when others  => if inputs(3) = '1' then
										est_futuro <= x1; 
									end if; 
			end case;
	end process;
	
	process (est_actual)
	begin
		case est_actual is 
				when x1 => estado <= "001";
				when x2 => estado <= "010";
				when x3 => estado <= "011";
				when x4 => estado <= "100";
				when x5 => estado <= "101";
				when x6 => estado <= "110";
				when others => estado <= "111"; 
	  end case;
	 end process;
end Behavioral;

