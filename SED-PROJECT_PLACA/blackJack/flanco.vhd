----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:59:10 01/14/2016 
-- Design Name: 
-- Module Name:    flanco - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flanco is
    Port ( input, clk : in  STD_LOGIC;
           pulse : out  STD_LOGIC);
end flanco;

architecture Behavioral of flanco is

	type estados is (x1,x2);
	signal est_actual:estados:=x1;
	signal est_futuro: estados;
	--signal pedir_temp : std_logic := '0';
	--signal contador : integer range 0 to 100:= 0;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			est_actual <= est_futuro;
		end if;
	end process;
	
	process(est_actual,input)
	begin
	pulse <= '0';
	--if rising_edge(clk) then
			case est_actual is 
				when x1 => if input = '1' then
										est_futuro <= x2;
										pulse <= '1';
									else 
										est_futuro <= x1;
										--temp <= '0';
							  end if;
				when x2 => 		pulse <= '0';
									if input = '0' then
										est_futuro <= x1;
									else
										est_futuro <= x2;
							  end if;	
			end case;
	--end if;
	end process;
	
end Behavioral;