--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:24:06 01/14/2016
-- Design Name:   
-- Module Name:   C:/.Xilinx/blackjack-modificado-fpga/blackJack/top_test.vhd
-- Project Name:  blackJack
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY top_test IS
END top_test;
 
ARCHITECTURE behavior OF top_test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT top
    PORT(
         play : IN  std_logic;
         R : IN  std_logic;
         plantarse : IN  std_logic;
         clk : IN  std_logic;
         display : OUT  std_logic_vector(6 downto 0);
         digctrl : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal play : std_logic := '0';
   signal R : std_logic := '0';
   signal plantarse : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal display : std_logic_vector(6 downto 0);
   signal digctrl : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;--frecuencia de 50MHz
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: top PORT MAP (
          play => play,
          R => R,
          plantarse => plantarse,
          clk => clk,
          display => display,
          digctrl => digctrl
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 1000 ms;	
		play <= '1';
      wait for 500 ms;
		play <= '0';
		wait for 1000 ms;
		
		play <= '1';
      wait for 500 ms;
		play <= '0';
		wait for 1000 ms;
		
		play <= '1';
      wait for 500 ms;
		play <= '0';
		wait for 1000 ms;
		
		play <= '1';
      wait for 500 ms;
		play <= '0';
		wait for 1000 ms;
		
		play <= '1';
      wait for 500 ms;
		play <= '0';
		wait for 1000 ms;
		
		R <= '1';
		wait for 500 ms;
		R <= '0';
      -- insert stimulus here 

      assert false
			report "Simulación finalizada"
			severity failure;
   end process;

END;
