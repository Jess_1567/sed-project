
-- VHDL Instantiation Created from source file SumadorJug2.vhd -- 13:31:48 01/25/2016
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT SumadorJug2
	PORT(
		play : IN std_logic;
		estado : IN std_logic_vector(2 downto 0);
		clk : IN std_logic;
		R : IN std_logic;
		sumando : IN std_logic_vector(3 downto 0);
		cambioJ : IN std_logic;          
		suma : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;

	Inst_SumadorJug2: SumadorJug2 PORT MAP(
		play => ,
		estado => ,
		clk => ,
		R => ,
		sumando => ,
		cambioJ => ,
		suma => 
	);


