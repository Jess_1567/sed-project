--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   00:50:45 01/04/2016
-- Design Name:   
-- Module Name:   /home/daaguirre/blackJack/binTobcd_tb.vhd
-- Project Name:  blackJack
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: binTobcd
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY binTobcd_tb IS
END binTobcd_tb;
 
ARCHITECTURE behavior OF binTobcd_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT binTobcd
    PORT(
         bin : IN  std_logic_vector(7 downto 0);
         bcd : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal bin : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal bcd : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant clock_period : time := 10 ns;
	signal clock : std_logic;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: binTobcd PORT MAP (
          bin => bin,
          bcd => bcd
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
	
	play_process : process
	begin
		wait for 40 ns;
		bin <= "00001100";
		wait for 40 ns;
		bin <= "00010101";
		wait for 40 ns;
	end process;

   -- Stimulus process
   stim_proc: process
   begin		
      wait for 200 ns;
      -- insert stimulus here 
		
		assert false
			report "Simulación finalizada"
			severity failure;
   end process;

END;
